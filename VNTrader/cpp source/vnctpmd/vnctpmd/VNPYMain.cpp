/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#pragma once
#include "stdafx.h"
#include "IniFile.h"
#include "Interface.h"
#include "MdSpi.h"
#include "IniFile.h"
#include "mapdef.h"
#include <hash_map>
#include <map>
#include <string>
#include <algorithm>
#include "iostream"
#include <iostream> 
#include <windows.h>
#include <thread>
#include <time.h>
using namespace std;

#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <vector>
#include <string>
#include <iosfwd>
#include <windows.h>
#include <conio.h>
#include <ctype.h>
using std::vector;
using std::string;
using std::wstring;
using std::iterator;
#ifdef UNICODE 
typedef wstring tstring;
#define lsprintf_s swprintf_s 
#else
typedef string tstring;
#define lsprintf_s sprintf_s 
#endif

#include<Iphlpapi.h>
#pragma comment(lib,"Iphlpapi.lib")
#include<Winsock2.h>
#pragma comment(lib,"Ws2_32.lib")

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS
#include <atlbase.h>
#include <atlstr.h>

#ifdef UNICODE
#define tcout wcout
#define tcin wcin
#else
#define tcout cout
#define tcin cin
#endif

KLineDataType klinedata[MAX_INSUTRUMENT][MAX_KLINE] = { 0 };
char* ppInstrumentID[MAX_INSUTRUMENT] = { 0 };
std::hash_map<std::string, int> gMarket;
//TradeTime
struct TradeTimesInterval
{
	double begin1;
	double end1;
	double begin2;
	double end2;
	double begin3;
	double end3;
};
TradeTimesInterval TradeTime[100];
std::string gFrontAddr[3];
std::string gBrokerID;
std::string gInvestorID;
std::string gPassword;

CThostFtdcDepthMarketDataField *depthdata[MAX_INSUTRUMENT] = {0};
int amount = 0;
//std::map<std::string, int> gMarket2;
std::string gTradeTime("TradeTime.ini");
int gStatus = 0;
extern CMdSpi vnmdspi;
struct Mdata
{
 int  processid;        //进程ID
 char InstrumentID[10]; //品种
};

unsigned nThreadID_OnFrontConnected;
unsigned nThreadID_OnFrontDisconnected;
unsigned nThreadID_OnRspUserLogin;
unsigned nThreadID_OnRspUserLogout;
unsigned nThreadID_OnRspQryInvestorPosition;
unsigned nThreadID_OnRspQryTradingAccount;
unsigned nThreadID_OnRtnOrder;
unsigned nThreadID_OnRtnTrade;
unsigned nThreadID_OnRspSubMarketData;
unsigned nThreadID_OnRspUnSubMarketData;
unsigned nThreadID_OnRtnDepthMarketData;

HANDLE hStartEvent_OnFrontConnected;
HANDLE hStartEvent_OnFrontDisconnected;
HANDLE hStartEvent_OnRspUserLogin;
HANDLE hStartEvent_OnRspUserLogout;
HANDLE hStartEvent_OnRspQryInvestorPosition;
HANDLE hStartEvent_OnRspQryTradingAccount;
HANDLE hStartEvent_OnRtnOrder;
HANDLE hStartEvent_OnRtnTrade;
HANDLE hStartEvent_OnRspSubMarketData;
HANDLE hStartEvent_OnRspUnSubMarketData;
HANDLE hStartEvent_OnRtnDepthMarketData;

HANDLE hMapFile = NULL;
LPCTSTR pBuf = NULL;
volatile KLineDataType iData;
int pid;
double  InitKlineData(char *InstrumentID)
{
	char sharename[60] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTrader%d%s", pid, InstrumentID);
	hMapFile = CreateFileMappingA(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		sizeof(KLineDataType)*MAX_KLINE,                // maximum object size (low-order DWORD)
		sharename);                 // name of mapping object

	if (hMapFile == NULL)
	{
		_tprintf(TEXT("Could not create file mapping object (%d).\n"),
			GetLastError());
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile,   // handle to map object
			FILE_MAP_ALL_ACCESS, // read/write permission
			0,
			0,
			sizeof(KLineDataType)*MAX_KLINE);
	}
	if (pBuf == NULL)
	{
		_tprintf(TEXT("Could not map view of file (%d).\n"),
			GetLastError());
		CloseHandle(hMapFile);
		return 0;
	}

	memset((PVOID)pBuf, 0, sizeof(KLineDataType)*MAX_KLINE);

	return 1;
}
void * GetKline(VNInstrument *a, int j)
{
	int id = gMarket[a->InstrumentID];
 
	if (gStatus)
	{
		return NULL;
	}
	if (id < 0 || id >= gMarket.size())
	{
		return NULL;
	}
	else
	{
		/*if (strcmp(klinedata[id][j].InstrumentID, "") != 0)
			printf("cpp21 [%d][%d][%s]  %.6f [%.2f][%.2f][%.2f][%.2f]\n", id, j, 
				klinedata[id][j].InstrumentID, klinedata[id][j].KlineTime, klinedata[id][j].High,
				klinedata[id][j].Low, klinedata[id][j].Open, klinedata[id][j].Close);*/

		/*
		size_t count = g_KlineHash[instrumentKey].m_KLineM1.size();
		if (count > 0)
		{
		if (gMarket[pDepthMarketData->InstrumentID] > MAX_INSUTRUMENT) { return; }
		memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][0], sizeof(KLineDataType), &g_KlineHash[instrumentKey].m_KLinelastM1, sizeof(KLineDataType));
		int j = 1;
		for (size_t i = 0; i < min(count, MAX_KLINE); i++)
		{
		memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][j], sizeof(KLineDataType), &g_KlineHash[instrumentKey].m_KLineM1[i], sizeof(KLineDataType));
		maxv = max(maxv, gMarket[pDepthMarketData->InstrumentID]);
		printf("g_KlineHash size: %d [%d] [max : %d]\n", (int)g_KlineHash[instrumentKey].m_KLineM1.size(), gMarket[pDepthMarketData->InstrumentID], maxv);
		printf("ss[%d]: %s %s %.6f  %.2f \n", (int)(i),
		pDepthMarketData->InstrumentID,
		g_KlineHash[instrumentKey].m_KLineM1[i].TradingDay,
		g_KlineHash[instrumentKey].m_KLineM1[i].KlineTime,
		g_KlineHash[instrumentKey].m_KLineM1[i].ClosePrice
		);
		j++;
		}
		}
		*/


		return  &klinedata[id][j];

	}
}

void * GetKline2(VNInstrument *a, int j )
{
	char sharename[100] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTrader%d%s", pid, a->InstrumentID);
	if (hMapFile == NULL)
	{
		hMapFile = OpenFileMappingA(
			FILE_MAP_ALL_ACCESS,   // read/write access
			FALSE,                 // do not inherit the name
			sharename);               // name of mapping object
	}
	if (hMapFile == NULL)
	{
		//_tprintf(TEXT("Could not open file mapping object (%d).\n"),
		//	GetLastError());
		//ReleaseMutex(hMutex2);
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
			FILE_MAP_ALL_ACCESS,  // read/write permission
			0,
			0,
			sizeof(KLineDataType)*MAX_KLINE);
	}
	if (pBuf == NULL)
	{
		//_tprintf(TEXT("Could not map view of file (%d).\n"),
		//	GetLastError());
		CloseHandle(hMapFile);
		//ReleaseMutex(hMutex2);
		return 0;
	}
	KLineDataType(*tn)[MAX_KLINE] = (KLineDataType(*)[MAX_KLINE])pBuf;
	/*
	for (int i = 0; i < 10; i++)
	{
		printf("[%d] %s , %f , %f\n", i, tn[0][i].InstrumentID, tn[0][i].KlineTime, tn[0][i].HighestPrice);
	}*/

	//int id = gMarket[a->InstrumentID];
	return  &tn[0][j];
}


void OpenLog()
{
	vnmdspi.showlog = true;
}

void CloseLog()
{
	vnmdspi.showlog = false;
}





#include "TickToKline.h"
extern hash_map<string, CTickToKline> g_KlineHash;
double  UpdateKline(string instrumentKey, int i, int id)
{
	char sharename[100] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTrader%d%s", pid, instrumentKey.c_str());
	if (hMapFile == NULL)
	{
		hMapFile = OpenFileMappingA(
			FILE_MAP_ALL_ACCESS,
			FALSE,
			sharename);
	}
	if (hMapFile == NULL)
	{
		//_tprintf(TEXT("Could not open file mapping object (%d).\n"),
		//	GetLastError());
		//ReleaseMutex(hMutex2);
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
			FILE_MAP_ALL_ACCESS,  // read/write permission
			0,
			0,
			sizeof(KLineDataType)*MAX_KLINE);
	}
	if (pBuf == NULL)
	{
		//_tprintf(TEXT("Could not map view of file (%d).\n"),
		//	GetLastError());
		CloseHandle(hMapFile);
		//ReleaseMutex(hMutex2);
		return 0;
	}
	KLineDataType(*tn)[MAX_KLINE] = (KLineDataType(*)[MAX_KLINE])pBuf;
	/*
	KLineDataType tt;
	memset(&tt, 0, sizeof(tt));
	tt.KlineTime = 0.1500;
	_snprintf_s(tt.InstrumentID, sizeof(tt.InstrumentID), sizeof(tt.InstrumentID) - 1, "%s", instrumentKey.c_str());
	memcpy_s((PVOID)(tn[0] + id), sizeof(KLineDataType), &tt, sizeof(KLineDataType));
	*/
	  memcpy_s((PVOID)(tn[0] + id), sizeof(KLineDataType), (const void *const)&(klinedata[i][id]), sizeof(KLineDataType));
	//ReleaseMutex(hMutex2);
	//UnmapViewOfFile(pBuf);
	//CloseHandle(hMapFile);
	return  1;
}

bool Start()
{
	return TRUE;
}

int UnSubscribeMarketData(VNInstrument  *InstrumentID)
{
	return 	vnmdspi.UnSubscribeMarketData();
	//char * crcvalue = NULL;
	//if (mapInstrument)
	//{
		//delete mapInstrument;
		//mapInstrument = NULL;
	//}
	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	memset(ppInstrumentID[amount],0,sizeof(TThostFtdcInstrumentIDType));
	::strcpy_s(ppInstrumentID[amount],sizeof(TThostFtdcInstrumentIDType), (char*)InstrumentID);
	//::strcpy_s(ppInstrumentID[amount],sizeof(ppInstrumentID[amount]), contract);
	//gMarket2[(char*)InstrumentID] = amount;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	memset(depthdata[amount],0,sizeof(CThostFtdcDepthMarketDataField));
	++amount;
	vnmdspi.UnSubscribeMarketData();
}

//询价
void SubscribeForQuoteRsp(  char *InstrumentID)
{
	/*
	char * crcvalue = NULL;

	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	::strcpy(ppInstrumentID[amount], InstrumentID);
	//::strcpy_s(ppInstrumentID[amount],sizeof(ppInstrumentID[amount]), contract);
	gMarket2[InstrumentID] = amount;
	//data[amount] = new CThostFtdcDepthMarketDataField;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	++amount;
	*/
	vnmdspi.SubscribeForQuoteRsp(InstrumentID);

}
//添加记录, 
bool FindId(const char *Instrument, const char * str)
{
	char * pdest1 = strstr((char*)Instrument, (char*)str);
	__int64  result1 = pdest1 - Instrument + 1;

	if (pdest1 != NULL)
	{		//printf("在%s发现%s\n", InstrumentID_n[id],str );
		return true;
	}
	else
	{
		//printf("%s 没有在%s发现\n", str, InstrumentID_n[id]);
		return false;
	}
}
extern void InitMarketdataSource(MarketdataSource * q);
 
int SubscribeMarketData(VNInstrument * a)
{
	InitKlineData(a->InstrumentID);

	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	//::strcpy(ppInstrumentID[amount], a->InstrumentID);
	::strcpy_s(ppInstrumentID[amount],sizeof(ppInstrumentID[amount]), a->InstrumentID);
	gMarket[a->InstrumentID] = amount;
	//klinedata[amount][0] = new KLineDataType;
	++amount;
	return mpUserApi->SubscribeMarketData(&(ppInstrumentID[amount - 1]), 1);
}


int SubscribeMarketData2(VNInstrument * a)
{
	InitKlineData(a->InstrumentID);


	int m = 1, n = 31;//行数和列数
	char ** ppthis = (char**)malloc(sizeof(char*)*m);//申请一组一维指针空间。
	for (int i = 0; i<m; i++)
		ppthis[i] = (char*)malloc(sizeof(char)*n);//对于每个一维指针，申请一行数据的空间。
	for (int i = 0; i<m; i++)//为每一个元素赋值
		for (int j = 0; j < n; j++)
		{
			ppthis[i][j] = 0;
		}
	_snprintf_s(&ppthis[0][0], 31, 30, "%s", a->InstrumentID);
	if (ppthis[0])
	{
		int iResult = mpUserApi->SubscribeMarketData(ppthis, 1);
	}
	return 0;

	/*
	int iResult = mpUserApi->SubscribeMarketData(&(ppInstrumentID[amount - 1]), 1);
	std::cout << ppInstrumentID[amount - 1] << std::endl;
	printf("SubscribeMarketData  %s\n", InstrumentID);
	//ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	//memset(ppInstrumentID[amount], 0, sizeof(TThostFtdcInstrumentIDType));
	//::strcpy_s(ppInstrumentID[amount], sizeof(TThostFtdcInstrumentIDType), (char*)InstrumentID);
	return vnmdspi.SubscribeMarketData((char*)(*InstrumentID));
	*/
	//return vnmdspi.SubscribeMarketData(wchar2char(InstrumentID));
	/*
	//char * crcvalue = NULL;
	Add(InstrumentID, NULL);
	ppInstrumentID[amount] = new TThostFtdcInstrumentIDType;
	::strcpy_s(ppInstrumentID[amount],sizeof(TThostFtdcInstrumentIDType), InstrumentID);
	//::strcpy_s(ppInstrumentID[amount],sizeof(ppInstrumentID[amount]), contract);
	gMarket2[InstrumentID] = amount;
	//data[amount] = new CThostFtdcDepthMarketDataField;
	depthdata[amount] = new CThostFtdcDepthMarketDataField;
	++amount;
	vnmdspi.SubscribeMarketData();
	*/
}


/*
int Test_MA()
{
	int retCode = TA_Initialize();
	if (retCode != TA_SUCCESS)
	{
		printf("\n初始化指标库失败\n");
		return -1;
	}

	printf("MD这是指标测试\n");


	TA_Real    closePrice[400] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
		41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,
		81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34,
		34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56,34,34,32,34,34};

	TA_Real    out[400];

	TA_Integer outBeg;

	//TA_IntegeroutNbElement;

	// ... initializeyour closing price here... 
	//double outNbElement[] = {34,34,56,21,33,45,567,78,8,9,12,34,55,66,89,111,34,56,78,22,0,34,35,36,37,78,34,34,33,22,345,64,3,34,56};
	int outNbElement = 0;
	retCode = TA_MA(0, 50,

		&closePrice[0],

		30, TA_MAType_SMA,

		&outBeg, &outNbElement, &out[0]);

	// The output isdisplayed here 

	for (int i = 0; i <outNbElement; i++)

		printf("第 %d 天 MA值 = %f\n", outBeg + i, out[i]);


	//int retValue = genCode(1, 1);

	retCode = TA_Shutdown();
	if (retCode != TA_SUCCESS)
	{
		printf("Shutdown failed (%d)\n", retCode);
	}
	return 1;
}
*/


bool CROSSUP(const char *InstrumentID, int indicators, int periodtype, int PriceType, int period1, int period2,bool printstate)
{
	return 0;// 屏蔽 2017.1.1 CxHashList->GetDataCross(InstrumentID, indicators, PriceType, periodtype, period1, period2, true, printstate);
}

bool CROSSDOWN(const char *InstrumentID, int indicators, int periodtype, int PriceType, int period1, int period2,bool printstate)
{
	return 0;//屏蔽2017.1.1 CxHashList->GetDataCross(InstrumentID, indicators, PriceType, periodtype, period1, period2, false, printstate);
 
}

bool CROSSUP_S(int strategyid, int periodtype, int PriceType, int period1, int period2)
{



	return 0;
}

bool CROSSDOWN_S(int strategyid, int periodtype, int PriceType, int period1, int period2)
{



	return 0;

}

bool End()
{

	//临界区
	//DeleteCriticalSection(&g_csdata);
	for (int i = 0; i < amount; ++i)
	{
		delete [](ppInstrumentID[i]);
		//delete klinedata[i];
		delete depthdata[i];
	}
	return true;
}

int IsInitOK()
{
	return vnmdspi.IsInitOK();
}

char *  GetApiVersion()
{
	return vnmdspi.GetApiVersion();
}

extern DWORD WINAPI QryThreadProc(void* p);	//更新排名
 
int InitMD()
{
	IniFile file;
	if (!file.Init("vnctpmd.ini"))
	{
		//读取vnctpmd.ini失败
		printf("md111111\n");

		return 1;
	}
	gBrokerID = file.GetValueFromSection("setting", "brokeid");
	gInvestorID = file.GetValueFromSection("setting", "investor");
	gPassword = file.GetValueFromSection("setting", "password");
	gFrontAddr[0] = file.GetValueFromSection("setting", "address1");
	gFrontAddr[1] = file.GetValueFromSection("setting", "address2");
	gFrontAddr[2] = file.GetValueFromSection("setting", "address3");

	if (gFrontAddr[0] == "" &&  gFrontAddr[1] == "" && gFrontAddr[2] == "")
	{
		//vnctpmd.ini中FrontAddr字段至少要设置一个
		printf("md22222\n");
		return 2;
	}

		char dir[256] = { 0 };
		::GetCurrentDirectory(255, dir);
		std::string tempDir = std::string(dir).append("\\MdTemp\\");
		::CreateDirectory(tempDir.c_str(), NULL);
		mpUserApi = CThostFtdcMdApi::CreateFtdcMdApi(tempDir.c_str());
		mpUserApi->RegisterSpi((CThostFtdcMdSpi*)&vnmdspi);
		mpUserApi->RegisterFront((char *)gFrontAddr[0].c_str());
		mpUserApi->RegisterFront((char *)gFrontAddr[1].c_str());
		mpUserApi->RegisterFront((char *)gFrontAddr[2].c_str());
		cerr << "--->>> " << (char *)gFrontAddr[0].c_str() << std::endl;
		cerr << "--->>> " << (char *)gFrontAddr[1].c_str() << std::endl;
		cerr << "--->>> " << (char *)gFrontAddr[2].c_str() << std::endl;
		mpUserApi->Init();
		HANDLE hThread = ::CreateThread(NULL, 0, QryThreadProc, NULL, 0, NULL);
		return 0;
}



char *  GetTradingDay()
{
	return vnmdspi.GetTradingDay();
}
void   RegisterFront(char *pszFrontAddress)
{
	return vnmdspi.RegisterFront(pszFrontAddress);
}

void VN_EXPORT RegisterNameServer(char *pszNsAddress)
{
	return vnmdspi.RegisterNameServer(pszNsAddress);
}

int ReqUserLogin()
{
	
	return vnmdspi.ReqUserLogin();
}

int ReqUserLogout()
{
	return vnmdspi.ReqUserLogout();
}



void Log(const char * filename, const char * content)
{
	//检查文件是否存在，是否需要新建文本文件
	ifstream inf;
	ofstream ouf;
	//inf.open(TickFileWritepaths[i], ios::out);
	inf.open(filename, ios::out);
	//}

	//记录TICK数据
	ofstream o_file(filename, ios::app);
	//if ( RunMode && ( check0 || check1 || check2) )

	//printf("xxxxxxxxxxxxxx%.06f\n",dbtoch(tick_data[id][1]));

	o_file << content <<  endl;

	//else
	//{
	//o_file << dbtoch(tick_data[i][1]) << "\t" << tick_data[2] << "\t" << Millisecs << "\t" << tick_AskPrice1[i][0] << "\t" << tick_AskVolume1[i][0] << "\t" << tick_BidPrice1[i][0] << "\t" << tick_BidVolume1[i][0] << "\t" << tick_data[i][4] << "\t" << dbtoch(tick_Volume[i][0]) << "\t" << dbtoch(tick_OpenInterest[i][0])<< endl; //将内容写入到文本文件中
	//}
	o_file.close();						//关闭文件
}

void TestArr(char** pIpAddList)
{
	for (int i = 0; i < 10; i++)
	{
		char temp[20] = {0};
		sprintf_s(temp,20,"%d",i);
		strcpy_s(pIpAddList[i],20, temp);
	    //ipAddress为IP地址
	}
}


void  SetRejectdataTime(double  begintime1, double endtime1, double begintime2, double endtime2, double begintime3, double endtime3, double begintime4, double endtime4)
{
	if (begintime1 < 0 || endtime1 < 0 || begintime2 < 0 || endtime2 < 0 || begintime3 < 0 || endtime3 < 0 || begintime4 < 0 || endtime4 < 0)
	{
		printf("设置拒收行情的时间段必须大于等于0\n");
	}

	if (begintime1 != 100 && endtime1 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime1, endtime1);
		vnmdspi.begintime1 = begintime1;
		vnmdspi.endtime1 = endtime1;
	}

	if (begintime2 != 100 && endtime2 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime2, endtime2);
	    vnmdspi.begintime2 = begintime2;
	    vnmdspi.endtime2 = endtime2;
	}

	if (begintime3 != 100 && endtime3 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime3, endtime3);
	  vnmdspi.begintime3 = begintime3;
	  vnmdspi.endtime3 = endtime3;

	}

	if (begintime3 != 100 && endtime3 != 100)
	{
		printf("[%0.06f ~ %0.06f]拒收数据的时间段\n", begintime4, endtime4);
	  vnmdspi.begintime4 = begintime4;
	  vnmdspi.endtime4 = endtime4;
	}

}


bool allprintfstate = false; //是否打印 tick数据

void VN_EXPORT SetPrintState(bool printfstate)
{
	allprintfstate = printfstate;
}

unsigned __stdcall MsgThreadOnFrontConnected(void *param)
{
	MSG msg;
	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!SetEvent(hStartEvent_OnFrontConnected))  // set thread start event 
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (GetMessage(&msg, 0, 0, 0))  // get msg from message queue
		{
			switch (msg.message)
			{
			 case MY_OnFrontConnected:
			 {
				((void(__cdecl *)(void))param)();
				if (vnmdspi.showlog)
					printf("recv MY_OnFrontConnected\n");
				break;
			 }
			}
		}
	};
}
unsigned __stdcall MsgThreadOnFrontDisconnected(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnFrontDisconnected)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0)) //get msg from message queue 
		{
			switch (msg.message)
			{
			case MY_OnFrontDisconnected:
			{
			    int reason = (int)msg.wParam;
				((void(__cdecl *)(int a))param)(reason);
				if (vnmdspi.showlog)
				   printf("Recv MsgOnFrontDisconnected %d\n", reason);
				break;
			}
			}
		}
	}
	return 0;
}
unsigned __stdcall MsgThreadOnRspUserLogin(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnRspUserLogin)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0))
		{
			switch (msg.message)
			{
			  case MY_OnRspUserLogin:
			  {
				CThostFtdcRspUserLoginField *obj = (CThostFtdcRspUserLoginField *)msg.wParam;
				((void(__cdecl *)(const CThostFtdcRspUserLoginField * a))param)(obj);
				delete obj;
				obj = NULL;
				if (vnmdspi.showlog)
				   printf("Recv MsgOnRspUserLogin\n");
				break;
			  }
			}
		}
	}
	return 0;
}
unsigned __stdcall MsgThreadOnRspUserLogout(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnRspUserLogout)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0)) //get msg from message queue 
		{
			switch (msg.message)
			{
			  case MY_OnRspUserLogout:
			  {
				CThostFtdcUserLogoutField * pUserLogout = (CThostFtdcUserLogoutField *)msg.wParam;
				((void(__cdecl *)(void))param)();
				delete pUserLogout;
				pUserLogout = NULL;
				if (vnmdspi.showlog)
					printf("Recv MsgOnRspUserLogout %s %s\n", pUserLogout->BrokerID, pUserLogout->UserID);
				break;
			  }
			}
		}
	}
	return 0;
}

unsigned __stdcall MsgThreadOnRspSubMarketData(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnRspSubMarketData)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0)) //get msg from message queue 
		{
			switch (msg.message)
			{
			case MY_OnRspSubMarketData:
			{
				CThostFtdcUserLogoutField * obj = (CThostFtdcUserLogoutField *)msg.wParam;
				((void(__cdecl *)(CThostFtdcUserLogoutField*))param)(obj);
				delete obj;
				obj = NULL;
				if (vnmdspi.showlog)
					printf("Recv MsgThreadOnRspSubMarketData\n");
				break;
			}
			}
		}
	}
	return 0;
}

//改
unsigned __stdcall MsgThreadOnRspUnSubMarketData(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnRspUnSubMarketData)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0)) //get msg from message queue 
		{
			switch (msg.message)
			{
			  case MY_OnRspUnSubMarketData:
			  {
				CThostFtdcUserLogoutField * obj = (CThostFtdcUserLogoutField *)msg.wParam;
				((void(__cdecl *)(CThostFtdcUserLogoutField*))param)(obj);
				delete obj;
				obj = NULL;
				if (vnmdspi.showlog)
					printf("Recv MsgThreadOnRspUnSubMarketData\n");
				break;
			  }
			}
		}
	}
	return 0;
}
bool test = 1;
unsigned __stdcall MsgThreadOnRtnDepthMarketData(void *param)
{
	MSG msg;
	::PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!::SetEvent(hStartEvent_OnRtnDepthMarketData)) //set thread start event
	{
		printf("set start event failed,errno:%d\n", ::GetLastError());
		return 1;
	}
	while (true)
	{
		if (::GetMessage(&msg, 0, 0, 0)) //get msg from message queue 
		{
			switch (msg.message)
			{
			case MY_OnRtnDepthMarketData:
			{
				CThostFtdcDepthMarketDataField * obj = (CThostFtdcDepthMarketDataField *)msg.wParam;
				((void(__cdecl *)(CThostFtdcDepthMarketDataField*))param)(obj);
				delete obj;
				obj = NULL;
				break;
			}
			}
		}
	}
	return 0;
}


void  VNRegOnFrontConnected(void(*outputCallback)())
{
	hStartEvent_OnFrontConnected = ::CreateEvent(0, FALSE, FALSE, 0);  // create thread start event
	if (hStartEvent_OnFrontConnected == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnFrontConnected, outputCallback, 0, &nThreadID_OnFrontConnected);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		CloseHandle(hStartEvent_OnFrontConnected);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnFrontConnected, INFINITE);
	CloseHandle(hStartEvent_OnFrontConnected);
	::WaitForSingleObject(hThread, INFINITE);
}

void   VNRegOnFrontDisconnected(void(*outputCallback)(int a))
{
	hStartEvent_OnFrontDisconnected = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnFrontDisconnected == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnFrontDisconnected, outputCallback, 0, &nThreadID_OnFrontDisconnected);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnFrontDisconnected);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnFrontDisconnected, INFINITE);
	::CloseHandle(hStartEvent_OnFrontDisconnected);
	::WaitForSingleObject(hThread, INFINITE);
}
 
void   VNRegOnRspUserLogin(void(*outputCallback)(const CThostFtdcRspUserLoginField* a))
{
	hStartEvent_OnRspUserLogin = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnRspUserLogin == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnRspUserLogin, outputCallback, 0, &nThreadID_OnRspUserLogin);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnRspUserLogin);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnRspUserLogin, INFINITE);
	::CloseHandle(hStartEvent_OnRspUserLogin);
	::WaitForSingleObject(hThread, INFINITE);
}
void   VNRegOnRspUserLogout(void(*outputCallback)(const CThostFtdcUserLogoutField * a))
{
	hStartEvent_OnRspUserLogout = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnRspUserLogout == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnRspUserLogout, outputCallback, 0, &nThreadID_OnRspUserLogout);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnRspUserLogout);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnRspUserLogout, INFINITE);
	::CloseHandle(hStartEvent_OnRspUserLogout);
	::WaitForSingleObject(hThread, INFINITE);
} 

void   VNRegOnRspSubMarketData(void(*outputCallback)(const TThostFtdcInstrumentIDType * a))
{
	return;
	hStartEvent_OnRspSubMarketData = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnRspSubMarketData == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnRspSubMarketData, outputCallback, 0, &nThreadID_OnRspSubMarketData);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnRspSubMarketData);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnRspSubMarketData, INFINITE);
	::CloseHandle(hStartEvent_OnRspSubMarketData);
	::WaitForSingleObject(hThread, INFINITE);
}

void   VNRegOnRspUnSubMarketData(void(*outputCallback)(const TThostFtdcInstrumentIDType * a))
{
	return;
	hStartEvent_OnRspUnSubMarketData = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnRspUnSubMarketData == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnRspUnSubMarketData, outputCallback, 0, &nThreadID_OnRspUnSubMarketData);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnRspUnSubMarketData);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnRspUnSubMarketData, INFINITE);
	::CloseHandle(hStartEvent_OnRspUnSubMarketData);
	::WaitForSingleObject(hThread, INFINITE);
}


void   VNRegOnRtnDepthMarketData(void(*outputCallback)(const CThostFtdcDepthMarketDataField* a))
{
	hStartEvent_OnRtnDepthMarketData = ::CreateEvent(0, FALSE, FALSE, 0); //create thread start event
	if (hStartEvent_OnRtnDepthMarketData == 0)
	{
		printf("create start event failed,errno:%d\n", ::GetLastError());
		return;
	}
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &MsgThreadOnRtnDepthMarketData, outputCallback, 0, &nThreadID_OnRtnDepthMarketData);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", ::GetLastError());
		::CloseHandle(hStartEvent_OnRtnDepthMarketData);
		return;
	}
	::WaitForSingleObject(hStartEvent_OnRtnDepthMarketData, INFINITE);
	::CloseHandle(hStartEvent_OnRtnDepthMarketData);
	::WaitForSingleObject(hThread, INFINITE);
}

int GetInstrumentNum()
{
	return  (int)gMarket.size();
}


int GetKlineMaxLen()
{
	return MAX_KLINE;
}

/*
::memcpy(&(data[StockIndex[pData->InstrumentID]]), pData,
	gStockDepthMarketDataSize);
*/

void * GetKlineData(int i,int j)
{
	if (gStatus)
	{
		return NULL;
	}
	if (i < 0 || i >= gMarket.size())
	{
		return NULL;
	}
	else
	{
		 if(strcmp(klinedata[i][j].InstrumentID,"")!=0)
		   printf("cpp [%d][%d][%s]  %.6f\n",i,j, klinedata[i][j].InstrumentID, klinedata[i][j].KlineTime);
		/*
		size_t count = g_KlineHash[instrumentKey].m_KLineM1.size();
		if (count > 0)
		{
			if (gMarket[pDepthMarketData->InstrumentID] > MAX_INSUTRUMENT) { return; }
			memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][0], sizeof(KLineDataType), &g_KlineHash[instrumentKey].m_KLinelastM1, sizeof(KLineDataType));
			int j = 1;
			for (size_t i = 0; i < min(count, MAX_KLINE); i++)
			{
				memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][j], sizeof(KLineDataType), &g_KlineHash[instrumentKey].m_KLineM1[i], sizeof(KLineDataType));
				maxv = max(maxv, gMarket[pDepthMarketData->InstrumentID]);
				printf("g_KlineHash size: %d [%d] [max : %d]\n", (int)g_KlineHash[instrumentKey].m_KLineM1.size(), gMarket[pDepthMarketData->InstrumentID], maxv);
				printf("ss[%d]: %s %s %.6f  %.2f \n", (int)(i),
					pDepthMarketData->InstrumentID,
					g_KlineHash[instrumentKey].m_KLineM1[i].TradingDay,
					g_KlineHash[instrumentKey].m_KLineM1[i].KlineTime,
					g_KlineHash[instrumentKey].m_KLineM1[i].ClosePrice
				);
				j++;
			}
		}
		*/

		//return  &klinedata[i][j];

		return  &klinedata[i][j];

	}
}