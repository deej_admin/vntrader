#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "CTP/ThostFtdcUserApiStruct.h"
#include "TickToKline.h"
#include <string>
using namespace std;
extern hash_map<string, CTickToKline> g_KlineHash;
void CTickToKline::KLineFromLocalData(const std::string &sFilePath, const std::string &dFilePath)
{
	// 先清理残留数据
	m_priceVec.clear();
	m_volumeVec.clear();
	m_KLineM1.clear();
	std::cout << "开始转换tick到k线..." << std::endl;
	// 默认读取的tick数据表有4个字段：合约代码、更新时间、最新价、成交量
	std::ifstream srcInFile;
	std::ofstream dstOutFile;
	srcInFile.open(sFilePath, std::ios::in);
	dstOutFile.open(dFilePath, std::ios::out);
	dstOutFile << "开盘价" << ','
		<< "最高价" << ','
		<< "最低价" << ','
		<< "收盘价" << ',' 
		<< "成交量" << std::endl;

	// 一遍解析文件一边计算k线数据，1分钟k线每次读取60 * 2 = 120行数据
	std::string lineStr;
	bool isFirstLine = true;
	while (std::getline(srcInFile, lineStr))
	{
		if (isFirstLine)
		{
			// 跳过第一行表头
			isFirstLine = false;
			continue;
		}
		std::istringstream ss(lineStr);
		std::string fieldStr;
		int count = 4;
		while (std::getline(ss, fieldStr, ','))
		{
			count--;
			if (count == 1)
				m_priceVec.push_back(std::atof(fieldStr.c_str()));
			else if (count == 0)
			{
				m_volumeVec.push_back(std::atoi(fieldStr.c_str()));
				break;
			}
		}

		char names[10] = { 0 };
		char times[10] = { 0 };
		strcpy_s(times, sizeof(times), "21:05:00");
		string str0 = names;
		string str1 = times;
		string str2 = times;
		string str3 = times;
		str1 = str1.substr(0, 2);
		str2 = str2.substr(3, 2);
		str3 = str3.substr(6, 2);
		int  hours = atoi(str1.c_str());
		int  minutes = atoi(str2.c_str());
		std::string instrumentKey = "rb2110";
		// 计算k线
		if (g_KlineHash[instrumentKey].lastmin != minutes)
		{
			KLineDataType k_line_data;
			k_line_data.Open = m_priceVec.front();
			k_line_data.High = *std::max_element(m_priceVec.cbegin(), m_priceVec.cend());
			k_line_data.Low = *std::min_element(m_priceVec.cbegin(), m_priceVec.cend());
			k_line_data.Close= m_priceVec.back();
			// 成交量的真实的算法是当前区间最后一个成交量减去上去一个区间最后一个成交量
			k_line_data.Volume = m_volumeVec.back() - m_volumeVec.front();
			//m_KLineM1.push_back(k_line_data); // 此处可以存到内存
			
			dstOutFile << k_line_data.Open << ','
				<< k_line_data.High<< ','
				<< k_line_data.Low << ','
				<< k_line_data.Close << ','
				<< k_line_data.Volume << std::endl;

			m_priceVec.clear();
			m_volumeVec.clear();
		}
	}
	srcInFile.close();
	dstOutFile.close();
}

bool CTickToKline::KLineM1FromTick(CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	m_priceVec.push_back(pDepthMarketData->LastPrice);
	m_volumeVec.push_back(pDepthMarketData->Volume);
	char names[10] = { 0 };
	char times[10] = { 0 };
	strcpy_s(times, sizeof(times), (char*)(&pDepthMarketData->UpdateTime));
	string str0 = names;
	string str1 = times;
	string str2 = times;
	string str3 = times;
	str1 = str1.substr(0, 2);
	str2 = str2.substr(3, 2);
	str3 = str3.substr(6, 2);
	int  hours = atoi(str1.c_str());
	int  minutes = atoi(str2.c_str());
	int  seconds = atoi(str3.c_str());
	double thistime = 0.01* hours + 0.0001* minutes + 0.000001*seconds;
	std::string instrumentKey = std::string(pDepthMarketData->InstrumentID);

	//printf("lastmin : %d minutes : %d\n",lastmin, minutes);
	if (g_KlineHash[instrumentKey].lastmin!= minutes)
	{
		g_KlineHash[instrumentKey].lastmin = minutes;
		KLineDataType k_line_data;
		memset(&k_line_data, 0, sizeof(KLineDataType));
		k_line_data.Open = m_priceVec.front();
		k_line_data.High = *std::max_element(m_priceVec.cbegin(), m_priceVec.cend());
		k_line_data.Low = *std::min_element(m_priceVec.cbegin(), m_priceVec.cend());
		k_line_data.Close = m_priceVec.back();
		// 成交量的真实的算法是当前区间最后一个成交量减去上去一个区间最后一个成交量
		k_line_data.Volume = m_volumeVec.back() - m_volumeVec.front();
		k_line_data.KlineTime = thistime;
		k_line_data.Minutes = minutes;
		_snprintf_s(k_line_data.InstrumentID,sizeof(k_line_data.InstrumentID), sizeof(k_line_data.InstrumentID)-1,"%s", pDepthMarketData->InstrumentID);
		_snprintf_s(k_line_data.TradingDay, sizeof(k_line_data.TradingDay), sizeof(k_line_data.TradingDay) - 1, "%s", pDepthMarketData->TradingDay);

	/*	for (int i=0;i<m_priceVec.size();i++)
		{
			printf("pvector (%d)[%zd][%.2f]\n", i, m_priceVec.size(), m_priceVec[i]);
			
		}

	 	printf("cpp31  %s %.6f [%.2f] [%.2f] [%.2f] [%.2f]\n" ,
			k_line_data.InstrumentID,  k_line_data.KlineTime, k_line_data.Open, k_line_data.High, k_line_data.Low, k_line_data.Close);

		if (k_line_data.High != k_line_data.Low)
		{
			printf("cppbd\n");
		} */

		m_KLineM1.push_back(k_line_data);
		if (m_KLineM1.size() > MAX_KLINE)
		{
			m_KLineM1.erase(m_KLineM1.begin());
		}

		
		m_priceVec.clear();
		m_volumeVec.clear();
		memset(&m_KLinelastM1, 0, sizeof(KLineDataType));
		return true;
	}
	else
	{



		if (m_KLinelastM1.Open < 1e-7)
		{
			m_KLinelastM1.Open = m_priceVec.front();
		}
		m_KLinelastM1.High = *std::max_element(m_priceVec.cbegin(), m_priceVec.cend());
		m_KLinelastM1.Low = *std::min_element(m_priceVec.cbegin(), m_priceVec.cend());
		m_KLinelastM1.Close = m_priceVec.back();
		// 成交量的真实的算法是当前区间最后一个成交量减去上去一个区间最后一个成交量
		m_KLinelastM1.Volume = m_volumeVec.back() - m_volumeVec.front();
		m_KLinelastM1.KlineTime = thistime;
		m_KLinelastM1.Minutes = minutes;
		return false;
	}
	return false;
}