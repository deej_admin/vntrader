/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#include "stdafx.h"
#include "TDSpi.h"
using namespace std;
extern std::string gFrontAddr[3];
extern std::string gBrokerID;
extern std::string gInvestorID;
extern std::string gPassword;
extern std::string gAppID;
extern std::string gAuthCode;
extern std::string gUserProductInfo;

CRITICAL_SECTION g_position;
CRITICAL_SECTION g_ontrade;
CRITICAL_SECTION g_onorder;

extern HANDLE ghTradedVolMutex;
extern std::map<int, int> gOrderRef2TradedVol;

//乘数
std::map<std::string, double> gUnderlyingMultiple;
char	InstrumentID_n[TYPE_NUM][10] = { 0 };


//保证金率
std::map<std::string, double> gMarginRate_long;
std::map<std::string, double> gMarginRate_short;
//手续费率
std::map<std::string, double> gCommissionRate;

//查询最大报单数量
std::map<std::string, int> gMaxOrderVolume;
  
//持仓map
std::map<std::string, int> gPosition_s;
std::map<std::string, int> gPosition_b;

std::map<std::string, int> gPosition_s_today;
std::map<std::string, int> gPosition_b_today;
std::map<std::string, int> gPosition_s_history;
std::map<std::string, int> gPosition_b_history;


std::map<std::string, int> gTypeCheckState_S_Today;
std::map<std::string, int> gTypeCheckState_B_Today;
std::map<std::string, int> gTypeCheckState_S_History;
std::map<std::string, int> gTypeCheckState_B_History;

CThostFtdcTraderApi *vntdapi;
CTDSpi  vntdspi;
//CShareMemoryPosition sp;

CTDSpi::CTDSpi()
{
	memset(positionptr, 0, sizeof(CThostFtdcInvestorPositionField)*positionnum);
	memset(&lasttradingday, 0, sizeof(TThostFtdcDateType));
	bInitOK = false;
	iRequestID = 0;
	iOrderRef = 0;
	FRONT_ID = 0;
	SESSION_ID = 0;
	vntdapi = NULL;
	hSyncObj = ::CreateEvent(NULL, FALSE, FALSE, NULL);
}

CTDSpi::~CTDSpi()
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (vntdapi)
	{
    // vntdapi->Release();
    // vntdapi = NULL;
	}
	::CloseHandle(hSyncObj);
	std::cout << "<--" << __FUNCTION__ << std::endl;
}

#include <ShellApi.h>
#include <iostream> 
#include <fstream> 
using namespace std;

bool connect = true;
bool GState = true;
bool test = 1;

DWORD WINAPI QryThreadProc(void* p)	//更新排名
{
	if (!test)
		return 0;
	while (true)
	{
		if (connect)
		{
			GState = !GState;
			if (GState)
			{
				vntdspi.ReqQryInvestorPosition();//查询仓位管理		
			}
			else
			{
				vntdspi.ReqQryTradingAccount(); //查询资金
			}
			if (0)
			{
			CThostFtdcInvestorPositionField obj;
			memset(&obj, 0, sizeof(CThostFtdcInvestorPositionField));
			obj.CashIn = 99.99;
			obj.Position = 5;
			obj.PosiDirection = 0;    //买卖方向
			obj.UseMargin = 5200;     //保证金
			obj.PositionCost = 3500;   //持仓成本
			obj.Position = 0;
			_snprintf_s(obj.TradingDay, sizeof(TThostFtdcDateType), sizeof(TThostFtdcDateType) - 1, "%s", "20210618");
			_snprintf_s(obj.InstrumentID, sizeof(TThostFtdcInstrumentIDType), sizeof(TThostFtdcInstrumentIDType) - 1, "%s", "rb2110");
			vntdspi.PMsg(nThreadID_OnRspQryInvestorPosition, MY_OnRspQryInvestorPosition, &obj, NULL, 0);
 

			CThostFtdcOrderField obj2;
			memset(&obj2, 0, sizeof(CThostFtdcOrderField));
			_snprintf_s(obj2.InstrumentID, sizeof(TThostFtdcInstrumentIDType), sizeof(TThostFtdcInstrumentIDType) - 1, "%s", "rb2110");
			_snprintf_s(obj2.OrderRef, sizeof(TThostFtdcOrderRefType), sizeof(TThostFtdcOrderRefType) - 1, "%s", "200001");
			_snprintf_s(obj2.InsertTime, sizeof(TThostFtdcTimeType), sizeof(TThostFtdcTimeType) - 1, "%s", "13:41:52");
			_snprintf_s(obj2.ExchangeID, sizeof(TThostFtdcExchangeIDType), sizeof(TThostFtdcExchangeIDType) - 1, "%s", "SHFE");
			obj2.LimitPrice = 3200;
			obj2.Direction = '0';
			_snprintf_s(obj2.CombOffsetFlag, sizeof(TThostFtdcCombOffsetFlagType), sizeof(TThostFtdcCombOffsetFlagType) - 1, "%s", "0");
			obj2.VolumeTotalOriginal = 3;
			obj2.VolumeTraded = 1;  //成交数量
			vntdspi.PMsg(nThreadID_OnRtnTrade, MY_OnRtnTrade, &obj2, NULL, 0);


			CThostFtdcTradeField obj3;
			memset(&obj3, 0, sizeof(CThostFtdcTradeField));
			_snprintf_s(obj3.InstrumentID, sizeof(TThostFtdcInstrumentIDType), sizeof(TThostFtdcInstrumentIDType) - 1, "%s", "rb2110");
			_snprintf_s(obj3.OrderRef, sizeof(TThostFtdcOrderRefType), sizeof(TThostFtdcOrderRefType) - 1, "%s", "200001");
			_snprintf_s(obj3.TradeID, sizeof(TThostFtdcTradeIDType), sizeof(TThostFtdcTradeIDType) - 1, "%s", "100001");
			obj3.Direction = '0';
			obj3.OffsetFlag = '0';
			obj3.Volume = 3;
			vntdspi.PMsg(nThreadID_OnRtnOrder, MY_OnRtnOrder, &obj3, NULL, 0);
			
			}
			
			//CTP有1S流控，太多的查询会导致查询失败
			Sleep(1001);
		}
	}
}

DWORD WINAPI ReqQryInstrumentMarginRateThreadProc(void* p)	//更新排名
{
	vntdspi.ReqQryInstrumentMarginRate("rb1701");//仓位管理		
	return 1;
}  

/*
bool CTDSpi::Init()
{

	char dir[256] = {0};
	//::ZeroMemory(dir, 256);
	::GetCurrentDirectory(255, dir);
	std::string tempDir = std::string(dir).append(".\\CTP\\");
	::CreateDirectory(tempDir.c_str(), NULL);

	vntdapi = CThostFtdcTraderApi::CreateFtdcTraderApi(".\\CTP\\");

	vntdspi = this;//自己增加
	vntdapi->RegisterSpi(this);
	vntdapi->SubscribePublicTopic(THOST_TERT_QUICK);
	vntdapi->SubscribePrivateTopic(THOST_TERT_QUICK);
	vntdapi->RegisterFront((char *)gFrontAddr[0].c_str());
	vntdapi->RegisterFront((char *)gFrontAddr[1].c_str());
	vntdapi->RegisterFront((char *)gFrontAddr[2].c_str());

	//std::cout << "QuickLib TD  CTP Init..." << std::endl;
	vntdapi->Init();
	DWORD err = ::WaitForSingleObject(hSyncObj, 10000);

	if (err == WAIT_OBJECT_0)
	{
		bInitOK = true;
	}
	//查询持仓线程
	HANDLE hThread3 = ::CreateThread(NULL, 0, QryThreadProc, NULL, 0, NULL);
	HANDLE hThread4 = ::CreateThread(NULL, 0, ReqQryInstrumentMarginRateThreadProc, NULL, 0, NULL);

	return bInitOK;


	return true;
}   
*/
int CTDSpi::ReqUserLogin()
{
	cerr << __FUNCTION__ << std::endl;
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(CThostFtdcReqUserLoginField));
	strcpy_s(req.BrokerID, sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(req.UserID, sizeof(TThostFtdcUserIDType), gInvestorID.c_str());
	strcpy_s(req.Password, sizeof(TThostFtdcPasswordType), gPassword.c_str());
	if (vntdapi)
	{
		return vntdapi->ReqUserLogin(&req, ++iRequestID);
	}
	else
	{
		return 1;
	}
}
extern HWND hThread;
void CTDSpi::PMsg(unsigned nThreadID, int msg, LPVOID p1, LPVOID p2,int Reason)
{
	switch (msg)
	{
	case MY_OnFrontConnected:
		if (!::PostThreadMessage(nThreadID, msg, 0, 0))
		{
			printf("post message(OnFrontConnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnFrontDisconnected:
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)Reason, NULL))
		{
			printf("post message(MY_OnFrontDisconnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnRspUserLogin:
	{
		CThostFtdcRspUserLoginField * t1 = new CThostFtdcRspUserLoginField;
		memset(t1, 0, sizeof(CThostFtdcRspUserLoginField));
		memcpy_s(t1, sizeof(CThostFtdcRspUserLoginField), p1, sizeof(CThostFtdcRspUserLoginField));

		//CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		//memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		//memcpy_s(t2, sizeof(CThostFtdcRspInfoField), p2, sizeof(CThostFtdcRspInfoField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			//delete t2;
			t1 = NULL;
			//t2 = NULL;
			printf("post message(MY_OnRspUserLogin) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspUserLogout:
	{
		CThostFtdcUserLogoutField * t1 = new CThostFtdcUserLogoutField;
		memset(t1, 0, sizeof(CThostFtdcUserLogoutField));
		memcpy_s(t1, sizeof(CThostFtdcUserLogoutField), p1, sizeof(CThostFtdcUserLogoutField));

		CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		memcpy_s(t2, sizeof(CThostFtdcRspInfoField), p2, sizeof(CThostFtdcRspInfoField));

		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			delete t2;
			t1 = NULL;
			t2 = NULL;
			printf("post message(MY_OnRspUserLogout) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspQryInvestorPosition:
	{
		CThostFtdcInvestorPositionField * t1 = new CThostFtdcInvestorPositionField;
		memset(t1, 0, sizeof(CThostFtdcInvestorPositionField));
		memcpy_s(t1, sizeof(CThostFtdcInvestorPositionField), p1, sizeof(CThostFtdcInvestorPositionField));
		//if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))

		//::SendMessage(hThread, msg, (WPARAM)t1, 0);		
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			printf("post message(OnRspQryInvestorPosition) failed, errno:%d\n", ::GetLastError());
			delete t1;
			t1 = NULL;
		}
		break;
	}
	case MY_OnRspQryTradingAccount:
	{
		VNDEFTradeAcount * t1 = new VNDEFTradeAcount;
		memset(t1, 0, sizeof(VNDEFTradeAcount));
		memcpy_s(t1, sizeof(VNDEFTradeAcount), p1, sizeof(VNDEFTradeAcount));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			printf("post message failed, errno:%d\n", ::GetLastError());
			delete t1;
			t1 = NULL;
		}
		break;
	}
	case MY_OnRtnOrder:
	{
		CThostFtdcOrderField * t1 = new CThostFtdcOrderField;
		memset(t1, 0, sizeof(CThostFtdcOrderField));
		memcpy_s(t1, sizeof(CThostFtdcOrderField), p1, sizeof(CThostFtdcOrderField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			printf("post message failed, errno:%d\n", ::GetLastError());
			delete t1;
			t1 = NULL;
		}
		break;
	}
	case MY_OnRtnTrade:
	{
		CThostFtdcTradeField * t1 = new CThostFtdcTradeField;
		memset(t1, 0, sizeof(CThostFtdcTradeField));
		memcpy_s(t1, sizeof(CThostFtdcTradeField), p1, sizeof(CThostFtdcTradeField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			printf("post message failed, errno:%d\n", ::GetLastError());
			delete t1;
			t1 = NULL;
		}
		break;
	}
	case MY_OnRtnDepthMarketData:
	{
		break;
	}
	case MY_OnRspSubMarketData:
	{
		break;
	}
	case MY_OnRspUnSubMarketData:
		break;
	case MY_OnRspForQuote:
	{
		break;
	}
	case MY_OnRspAuthenticate:
	{
		break;
	}
	case MY_IsErrorRspInfo:
	{
		break;
	}
	}
}

extern unsigned nThreadID;
void CTDSpi::OnFrontConnected()
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	connect = true;
	//认证请求
	ReqAuthenticate();
	PMsg(nThreadID_OnFrontConnected, MY_OnFrontConnected,NULL, NULL, 0);

}
int CTDSpi::ReqAuthenticate()
{
	cerr << __FUNCTION__ << std::endl;
	if (vntdapi == NULL)
	{
		return 1;
	}
	CThostFtdcReqAuthenticateField  req;
	memset(&req, 0, sizeof(CThostFtdcReqAuthenticateField));
	_snprintf_s(req.BrokerID, sizeof(req.BrokerID), sizeof(req.BrokerID) - 1, "%s", gBrokerID.c_str());
	_snprintf_s(req.UserID, sizeof(req.UserID), sizeof(req.UserID) - 1, "%s", gInvestorID.c_str());
	_snprintf_s(req.AppID, sizeof(req.AppID), sizeof(req.AppID) - 1, "%s", gAppID.c_str());
	_snprintf_s(req.AuthCode, sizeof(req.AuthCode), sizeof(req.AuthCode) - 1, "%s", gAuthCode.c_str());
	_snprintf_s(req.UserProductInfo, sizeof(req.UserProductInfo), sizeof(req.UserProductInfo) - 1, "%s", gUserProductInfo.c_str());
	printf("%s,%s,%s,%s,%s\n", req.BrokerID, req.UserID, req.AppID, req.AuthCode, req.UserProductInfo);
	return vntdapi->ReqAuthenticate(&req, ++iRequestID);
}

void CTDSpi::OnRspAuthenticate(CThostFtdcRspAuthenticateField *pRspAuthenticateField, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{	
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	/*if (pRspAuthenticateField == NULL)
	{
		ReqAuthenticate();
		return;
	}
	if (IsErrorRspInfo(pRspInfo))
	{
		std::cout << "OnRspAuthenticate\n" << std::endl;
	}
    */
	ReqUserLogin();
}
 
void CTDSpi::OnFrontDisconnected(int nReason)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnFrontDisconnected, MY_OnFrontDisconnected,NULL, NULL, nReason);
	connect = false;
	SYSTEMTIME t;
	::GetLocalTime(&t);
	std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
	std::cout << "--->>> " << __FUNCTION__ << std::endl;
	std::cout << "--->>> Reason = " << nReason << std::endl;
	::Beep(450, 500);
}

void CTDSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{	
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pRspUserLogin==NULL)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogin, MY_OnRspUserLogin, pRspUserLogin, pRspInfo, nRequestID);
 
	if (strcmp(pRspUserLogin->TradingDay, lasttradingday) != 0)
	{
		_snprintf_s(lasttradingday,sizeof(TThostFtdcDateType), sizeof(TThostFtdcDateType)-1,"%s", pRspUserLogin->TradingDay);
		EnterCriticalSection(&g_position);
		map_position.clear();
		LeaveCriticalSection(&g_position);
	}
	/*

	FRONT_ID = pRspUserLogin->FrontID;
	SESSION_ID = pRspUserLogin->SessionID;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			printf("Failer:ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
		}
		else
		{
			printf("Scuess:\n");
		}
	}

	*/

 	int GetLocalTimeMs2();
	char times[10] = { 0 };
	strncpy_s(times, sizeof(times), pRspUserLogin->SHFETime, sizeof(pRspUserLogin->SHFETime));
	string str1 = times;
	string str2 = times;
	string str3 = times;
	try
	{
		int hours = atoi(str1.c_str());
		int minutes = atoi(str2.c_str());
		int seconds = atoi(str3.c_str());
		int Millisecs = 0;
	}
	catch (exception &)
	{
		//cout << "--->>>时间转换错误\n" << endl;
	}

	//BATtime和本地时间差，用于估算分钟开盘时间，刚登录时采用与上期所时间差进行初始化。
	//Timeoffset_Tick=(((hours*60*60+minutes*60+seconds)*1000+Millisecs)-GetLocalTimeMs2());
	if (IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo->ErrorID != 3 && pRspInfo->ErrorID != 75)
		{
			//3密码错误，75密码错误超限
			Sleep(3000);
			ReqAuthenticate();
			ReqUserLogin();
		}
		else
		{
			printf("error password\n");
		}
	}

	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		// 保存会话参数
		FRONT_ID = pRspUserLogin->FrontID;
		SESSION_ID = pRspUserLogin->SessionID;
		int iNextOrderRef = atoi(pRspUserLogin->MaxOrderRef);
		iNextOrderRef++;
		//sprintf(ORDER_REF, "%d", iNextOrderRef);
		//temp.Format(_T("交易登录认证成功[%s_%s]"), BROKER_IDstr.GetBuffer(BROKER_IDstr.GetLength()), INVESTOR_IDstr.GetBuffer(INVESTOR_IDstr.GetLength()));
		printf("OnRspUserLogin scuess\n");
		///投资者结算结果确认
		ReqSettlementInfoConfirm();
		ReqSettlementInfoConfirm();

	}

}
 
void CTDSpi::OnRspQryInstrument2(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pInstrument==NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			printf("failer:OnRspQryInstrument2\n");
		}
	}
}

void CTDSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pUserLogout==NULL)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogout, MY_OnRspUserLogout, pUserLogout, pRspInfo, nRequestID);
	//FRONT_ID = pUserLogout->FrontID;
	//SESSION_ID = pUserLogout->SessionID;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo && pRspInfo->ErrorID != 0)
		{
			printf("OnRspUserLogout Failer,ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
		}
		else
		{
			printf("OnRspUserLogout Scuess\n");
		}
	}
}

int CTDSpi::ReqSettlementInfoConfirm()
{
	//投资者结算结果确认
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	CThostFtdcSettlementInfoConfirmField req;
	memset(&req, 0, sizeof(CThostFtdcSettlementInfoConfirmField));
	strcpy_s(req.BrokerID,sizeof(req.BrokerID), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(req.InvestorID), gInvestorID.c_str());
	printf("%s,%s\n", req.BrokerID, req.InvestorID);
	return vntdapi->ReqSettlementInfoConfirm(&req, ++iRequestID);
}

void CTDSpi::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	//if (bIsLast)
	//{
		//::SetEvent(hSyncObj);
		//确认结算单成功
	//}
}

bool CTDSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		SYSTEMTIME t;
		::GetLocalTime(&t);
		std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
		std::cout << "--->>>td ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << std::endl;
	}
	return bResult;
}

void CTDSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	std::cerr << pRspInfo->ErrorID << "\t" << pRspInfo->ErrorMsg << std::endl;
	IsErrorRspInfo(pRspInfo);
}

void CTDSpi::OnRtnOrder(CThostFtdcOrderField *pOrder)
{
	//if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (!pOrder)
	{
		return;
	}
	PMsg(nThreadID_OnRtnOrder, MY_OnRtnOrder, pOrder, NULL, 0);
	printf("OnRtnOrder:[%s][%s][%s]\n", pOrder->OrderRef, pOrder->InstrumentID, pOrder->ExchangeID);
		int orderRef = ::atoi(pOrder->OrderRef);
		::WaitForSingleObject(ghTradedVolMutex, INFINITE);
		gOrderRef2TradedVol[orderRef] = pOrder->VolumeTraded;
		::ReleaseMutex(ghTradedVolMutex);

	EnterCriticalSection(&g_onorder);
	char keystr[40] = { 0 };
	_snprintf_s(keystr, sizeof(keystr), sizeof(keystr) - 1, "%s_%d_%s_%d_%s",           //报单引用,请求编号,报单编号
		pOrder->InstrumentID, pOrder->VolumeTraded, pOrder->OrderRef, pOrder->RequestID, pOrder->OrderSysID);
	if (map_onorder.find(keystr) != map_onorder.end())
	{
			//找到
	}
	else
	{
			// 没找到该合约 
			if (tid < onordernum)
			{
				memcpy_s((PVOID)(&onorderptr[0] + tid), sizeof(CThostFtdcOrderField), pOrder, sizeof(CThostFtdcOrderField));
				tid++;
			}
			pair<string, int>value(keystr, tid - 1);
			map_onorder.insert(value);
	}
	LeaveCriticalSection(&g_onorder);
}

void CTDSpi::OnRtnTrade(CThostFtdcTradeField *pTrade)
{
	//if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (!pTrade)
	{
		return;
	}
	PMsg(nThreadID_OnRtnTrade, MY_OnRtnTrade, pTrade, NULL, 0);
	printf("OnRtnTrade:[%s][%s][%s]\n", pTrade->OrderRef, pTrade->InstrumentID, pTrade->ExchangeID);


	EnterCriticalSection(&g_ontrade);
	char keystr[40] = { 0 };
	_snprintf_s(keystr, sizeof(keystr), sizeof(keystr) - 1, "%s_%d_%s_%s_%s",           //报单引用,成交编号,报单编号
		pTrade->InstrumentID, pTrade->Volume, pTrade->OrderRef, pTrade->TradeID, pTrade->OrderSysID);
	if (map_ontrade.find(keystr) != map_ontrade.end())
	{
		//找到
	}
	else
	{
		// 没找到该合约 
		if (tid < ontradenum)
		{
			memcpy_s((PVOID)(&ontradeptr[0] + tid), sizeof(CThostFtdcTradeField), pTrade, sizeof(CThostFtdcTradeField));
			tid++;
		}

		pair<string, int>value(keystr, tid - 1);
		map_ontrade.insert(value);
	}
	LeaveCriticalSection(&g_ontrade);
	//int orderRef = ::atoi(pTrade->OrderRef);
	/*
	::WaitForSingleObject(ghTradedVolMutex, INFINITE);
	gOrderRef2TradedVol[orderRef] = pTrade->VolumeTraded;
	::ReleaseMutex(ghTradedVolMutex);
	*/
}

//合约交易状态通知
 
void  CTDSpi::OnRtnInstrumentStatus(CThostFtdcInstrumentStatusField *pInstrumentStatus)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (!pInstrumentStatus)
	{
		return;
	}
}

int CTDSpi::DeleteOrder(char *InstrumentID, DWORD orderRef)
{
	//错误返回-1是和正常冲突的吗？
	if (!InstrumentID)
	{
		return -1;
	}
	//cerr << __FUNCTION__ << std::endl;
	CThostFtdcInputOrderActionField ReqDel;
	::ZeroMemory(&ReqDel, sizeof(ReqDel));
	strcpy_s(ReqDel.BrokerID,sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(ReqDel.InvestorID,sizeof(TThostFtdcInvestorIDType),gInvestorID.c_str());
	strcpy_s(ReqDel.InstrumentID,sizeof(TThostFtdcInstrumentIDType), InstrumentID);
	_snprintf_s(ReqDel.OrderRef,sizeof(TThostFtdcOrderRefType), sizeof(TThostFtdcOrderRefType)-1, "%012d", orderRef);
	ReqDel.FrontID = FRONT_ID;
	ReqDel.SessionID = SESSION_ID;
	ReqDel.ActionFlag = THOST_FTDC_AF_Delete;
	int iResult = vntdapi->ReqOrderAction(&ReqDel, ++(iRequestID));

	if (iResult != 0)
		cerr << "Failer: 撤单 : " << ((iResult == 0) ? "成功" : "失败(") << iResult << ")" << endl;
	else
		cerr << "Scuess: 撤单 : 成功" << endl;
	return iResult;
}

VOID MakeOrder(CThostFtdcInputOrderField *pOrder)
{
	memset(pOrder, 0, sizeof(*pOrder));
	///经纪公司代码
	strcpy_s(pOrder->BrokerID, sizeof(TThostFtdcBrokerIDType),gBrokerID.c_str());
	///投资者代码
	strcpy_s(pOrder->InvestorID, sizeof(TThostFtdcInvestorIDType),gInvestorID.c_str());
	///用户代码
	//TThostFtdcUserIDType	UserID;
	///组合投机套保标志
	pOrder->CombHedgeFlag[0] = THOST_FTDC_HF_Speculation;	
	///限价单
	pOrder->OrderPriceType = THOST_FTDC_OPT_LimitPrice;
	///触发条件: 立即
	pOrder->ContingentCondition = THOST_FTDC_CC_Immediately;
	///有效期类型: 当日有效
	pOrder->TimeCondition = THOST_FTDC_TC_GFD;
	///GTD日期
	//	TThostFtdcDateType	GTDDate;
	///成交量类型: 任何数量
	pOrder->VolumeCondition = THOST_FTDC_VC_AV;
	///最小成交量: 1
	pOrder->MinVolume = 1;
	///止损价
	//	TThostFtdcPriceType	StopPrice;
	///强平原因: 非强平
	pOrder->ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
	///自动挂起标志: 否
	pOrder->IsAutoSuspend = 0;
	///业务单元
	//	TThostFtdcBusinessUnitType	BusinessUnit;
	///请求编号
	//	TThostFtdcRequestIDType	RequestID;
	///用户强评标志: 否
	pOrder->UserForceClose = 0;
}


int CTDSpi::InsertOrder(char *InstrumentID, char *ExchangeID, char Direction,char OffsetFlag, char PriceType, double Price, int VolumeTotalOriginal)
{
	CThostFtdcInputOrderField req;
	memset(&req,0,sizeof(CThostFtdcInputOrderField));
	//MakeOrder(&req);
	///经纪公司代码
	::strcpy_s(req.BrokerID, sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	///投资者代码
	::strcpy_s(req.InvestorID, sizeof(TThostFtdcBrokerIDType), gInvestorID.c_str());
	///合约代码
	::strcpy_s(req.InstrumentID, sizeof(TThostFtdcInstrumentIDType), InstrumentID);
	///交易所
	::strcpy_s(req.ExchangeID, sizeof(TThostFtdcExchangeIDType), ExchangeID);
	///报单引用
	++(iOrderRef);
	_snprintf_s(req.OrderRef, sizeof(TThostFtdcOrderRefType), sizeof(TThostFtdcOrderRefType) - 1, "%012d", iOrderRef);
	///报单价格条件: 限价
	req.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
	//req.OrderPriceType = PriceType;
	///买卖方向: 
	req.Direction = Direction;
	///组合开平标志: 开仓
	req.CombOffsetFlag[0] = OffsetFlag;
	///组合投机套保标志
	req.CombHedgeFlag[0] = THOST_FTDC_HF_Speculation;
	///价格
	req.LimitPrice = Price;
	///数量: 1
	req.VolumeTotalOriginal = VolumeTotalOriginal;
	///有效期类型: 当日有效
	req.TimeCondition = THOST_FTDC_TC_GFD;
	///GTD日期
	//	TThostFtdcDateType	GTDDate;
	///成交量类型: 任何数量
	req.VolumeCondition = THOST_FTDC_VC_AV;
	///最小成交量: 1
	req.MinVolume = 1;
	///触发条件: 立即
	req.ContingentCondition = THOST_FTDC_CC_Immediately;
	///止损价
	//	TThostFtdcPriceType	StopPrice;
	///强平原因: 非强平
	req.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
	///自动挂起标志: 否
	req.IsAutoSuspend = 0;
	///业务单元
	//	TThostFtdcBusinessUnitType	BusinessUnit;
	///请求编号
	//	TThostFtdcRequestIDType	RequestID;
	///用户强评标志: 否
	req.UserForceClose = 0;
	return vntdapi->ReqOrderInsert(&req, ++iRequestID);
}

int CTDSpi::ReqQryInvestorPosition()
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (vntdapi == NULL)
	{
		return 1;
	}
	// WirteTradeRecordToFileMainThread(0, "ReqQryInvestorPosition");
	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);
	CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(CThostFtdcQryInvestorPositionField));
	//strcpy(req.BrokerID, BROKER_ID);
	//strcpy(req.InvestorID, INVESTOR_ID);
	strcpy_s(req.BrokerID,sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(TThostFtdcInvestorIDType),gInvestorID.c_str());
	//strcpy(req.InstrumentID, INSTRUMENT_ID);
	//printf("指定持仓%s", INSTRUMENT_ID);
	//int iResult = vntdapi->ReqQryInvestorPosition(&req, ++iRequestID);
	return vntdapi->ReqQryInvestorPosition(&req, ++iRequestID);
	//cerr << "请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败") << endl;
	//if (iResult != 0)
	//	cerr << "Failer(ReqQryInvestorPosition): 请求查询投资者持仓: " << ((iResult == 0) ? "成功" : "失败(") << iResult << ")" << endl;
	//else
	//	cerr << "Scuess(ReqQryInvestorPosition): 请求查询投资者持仓: 成功" << endl;

}

///请求查询投资者品种/跨品种保证金
int CTDSpi::ReqQryInvestorProductGroupMargin(char *Instrument)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;

	if (vntdapi == NULL)
	{
		return 1;
	}
	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);
	//CThostFtdcQryInvestorProductGroupMarginField
	/*
	///查询投资者品种/跨品种保证金
	struct CThostFtdcQryInvestorProductGroupMarginField
	{
		///经纪公司代码
		TThostFtdcBrokerIDType	BrokerID;
		///投资者代码
		TThostFtdcInvestorIDType	InvestorID;
		///品种/跨品种标示
		TThostFtdcInstrumentIDType	ProductGroupID;
		///投机套保标志
		TThostFtdcHedgeFlagType	HedgeFlag;
	};
	*/
	CThostFtdcQryInvestorProductGroupMarginField req;
	//CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(CThostFtdcQryInvestorProductGroupMarginField));

	strcpy_s(req.BrokerID,sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(TThostFtdcInvestorIDType), gInvestorID.c_str());
	req.HedgeFlag = '1';
 	return vntdapi->ReqQryInvestorProductGroupMargin(&req, ++iRequestID);
}


int CTDSpi::ReqQueryMaxOrderVolume(CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, int nRequestID)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	return vntdapi->ReqQueryMaxOrderVolume(pQueryMaxOrderVolume, ++iRequestID);
}

///期货发起银行资金转期货请求
int CTDSpi::ReqFromBankToFutureByFuture(CThostFtdcReqTransferField *pReqTransfer, int nRequestID)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	return vntdapi->ReqFromBankToFutureByFuture(pReqTransfer, ++iRequestID);
}

///期货发起期货资金转银行请求
int CTDSpi::ReqFromFutureToBankByFuture(CThostFtdcReqTransferField *pReqTransfer, int nRequestID)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	return vntdapi->ReqFromFutureToBankByFuture(pReqTransfer, ++iRequestID);
}

int CTDSpi::ReqQryInstrument(char *Instrument)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (vntdapi == NULL)
	{
		return 1;
	}
	CThostFtdcQryInstrumentField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
	strcpy_s(req.ExchangeID,sizeof(TThostFtdcExchangeIDType), "DCE");
	strcpy_s(req.ExchangeInstID,sizeof(TThostFtdcExchangeInstIDType), "");
	strcpy_s(req.ProductID,sizeof(TThostFtdcInstrumentIDType), "");
	//strcpy(req.BrokerID, gBrokerID.c_str());
	//strcpy(req.InvestorID, gInvestorID.c_str());
	strcpy_s(req.InstrumentID,sizeof(TThostFtdcInstrumentIDType), Instrument);
	//printf("查询乘数[%s]   [%s]  [%s]", req.ExchangeID, req.ProductID, req.InstrumentID);
	return vntdapi->ReqQryInstrument(&req, ++iRequestID);
}

int CTDSpi::ReqQryInstrumentMarginRate(char *Instrument)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (vntdapi == NULL)
	{
		return 1;
	}
	//CThostFtdcQryInvestorPositionField req = { 0 };
	//strcpy(req.BrokerID, m_BrokerID);
	//strcpy(req.InvestorID, m_InvestorInfos[reqInfo.lAccIdx].InvestorID);
	//req.InstrumentID; //指定合约的话，就是查询特定合约的持仓信息，不填就是查询所有持仓  
	//ReqQryInvestorPosition(&req, reqInfo.nRequestID);
	CThostFtdcQryInstrumentMarginRateField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentMarginRateField));
	strcpy_s(req.BrokerID,sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(TThostFtdcInvestorIDType), gInvestorID.c_str());
	strcpy_s(req.InstrumentID,sizeof(TThostFtdcInstrumentIDType), Instrument);
	req.HedgeFlag = '1';
	printf("查询保证金[%s] 投资者账户[%s]合约[%s]\n",req.BrokerID, req.InvestorID, req.InstrumentID);
	//1>CTPTraderSpi.cpp(654) : error C2664 : “int CThostFtdcTraderApi::ReqQryInstrumentMarginRate(
		//CThostFtdcQryInstrumentMarginRateField *, int)” : 无法将参数 1 从“
		//CThostFtdcInstrumentMarginRateField *”转换为“CThostFtdcQryInstrumentMarginRateField *”
	//printf("\n----------------------------------------\n");
	//strcpy(req.InstrumentID, INSTRUMENT_ID);
	//printf("指定持仓%s", INSTRUMENT_ID);
	return vntdapi->ReqQryInstrumentMarginRate(&req, ++iRequestID);
}

///请求查询合约
int CTDSpi::ReqQryInstrument(CThostFtdcQryInstrumentField *pQryInstrument, int nRequestID)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pQryInstrument == NULL)
	{
		return 0;
	}
	CThostFtdcQryInstrumentField req;
	memset(&req, 0, sizeof(CThostFtdcQryInstrumentField));
	//strcpy(req.BrokerID, gBrokerID.c_str());
	//strcpy(req.InvestorID, gInvestorID.c_str());
	//strcpy(req.InstrumentID, Instrument);
	return vntdapi->ReqQryInstrument(&req, ++iRequestID);
}

int CTDSpi::ReqQryContractBank(CThostFtdcQryContractBankField *pQryContractBank, int nRequestID)
{

	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pQryContractBank == NULL)
	{
		return -1;
	}
	CThostFtdcQryContractBankField req;
	memset(&req, 0, sizeof(CThostFtdcQryContractBankField));
	//strcpy(req.BrokerID, gBrokerID.c_str());
	//strcpy(req.InvestorID, gInvestorID.c_str());
	//strcpy(req.InstrumentID, Instrument);
	int iResult = vntdapi->ReqQryContractBank(&req, ++iRequestID);
	// cerr << "Failer: 请求查询银行: " << ((iResult == 0) ? "成功" : "失败(") << iResult<<")"<<endl;
	if(iResult!=0)
	    cerr << "Failer(ReqQryContractBank): 请求查询银行: " << ((iResult == 0) ? "成功" : "失败(") << iResult<<")"<<endl;
	else
		cerr << "Scuess(ReqQryContractBank): 请求查询银行: 成功" <<endl;
	return iResult;
}

int CTDSpi::ReqQryTradingAccount()
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;

	if (vntdapi == NULL)
	{
		return 1;
	}
	CThostFtdcQryTradingAccountField req;
	memset(&req, 0, sizeof(CThostFtdcQryTradingAccountField));
	strcpy_s(req.BrokerID,sizeof(TThostFtdcBrokerIDType), gBrokerID.c_str());
	strcpy_s(req.InvestorID,sizeof(TThostFtdcInvestorIDType), gInvestorID.c_str());
	return vntdapi->ReqQryTradingAccount(&req, ++iRequestID); 
}

bool FindStr(int id, char * str)
{
	if (_stricmp(InstrumentID_n[id], str) == 0)
	{	
		//找到
		return true;
	}
	else
	{
		//未找到
		return false;
	}
}

int SaveInstrumentID = { 0 };
bool  checkstate = false;
bool  TypeCheckState_B_Today[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
bool  TypeCheckState_S_Today[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

bool  TypeCheckState_B_History[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
bool  TypeCheckState_S_History[TYPE_NUM] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
///请求查询投资者持仓响应

int errnum = 0;
extern bool showpositionstate;


void CTDSpi::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pInvestorPosition == NULL)
	{
		return;
	}
/*
	[ID]22合约:m1609持仓方向:2仓位 : 0BrokerID : 0AbandonFrozen : 8016CashIn : 0
	a : 162540
	b : 780
	c : 780
	d : 120
	e : 6
	f : 0
	g : 0
	h : 0
	i : 9.71378
	j : 0
	k : 0l : 0cc : 0
	m : 1
	n : m1609
	o : 13100293
	p : 0
	q : 0
	r : 0.1
	s : 0
	t : 0
	u : 0
	v : 0
	w : 2
	x : 0
	y : 0
	z :
	11 : 0
	2 : 0
	3 : 2696
	4 : 1
	5 : 2709
	6 : 0
	7 : 0
	8:0
	9:0
	10:0
	11 : 0
	12 : 0
	13 : 20160504
	14 : 0
	15 : 6
	*/
	while (nRequestID != SaveInstrumentID)
	{
		SaveInstrumentID = nRequestID;
		gTypeCheckState_B_History[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_B_Today[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_S_History[pInvestorPosition->InstrumentID] = false;
		gTypeCheckState_S_Today[pInvestorPosition->InstrumentID] = false;
	}

	if (pInvestorPosition->PosiDirection == '2' &&  pInvestorPosition->Position != 0) //注意别的版本要修复
	{
		if (pInvestorPosition->TodayPosition == 0) //今仓
		{
			gTypeCheckState_B_History[pInvestorPosition->InstrumentID] = true;
			gPosition_b_history[pInvestorPosition->InstrumentID] = pInvestorPosition->Position;
			//历史买单
		}
		else
		{
			gTypeCheckState_B_Today[pInvestorPosition->InstrumentID] = true;
			gPosition_b_today[pInvestorPosition->InstrumentID] = pInvestorPosition->Position;
			//今仓买单
		}
	}
	else if (pInvestorPosition->PosiDirection == '3' &&  pInvestorPosition->Position != 0)  //注意别的版本要修复
	{
		if (pInvestorPosition->TodayPosition == 0) //今仓
		{
			gTypeCheckState_S_History[pInvestorPosition->InstrumentID] = true;
			gPosition_s_history[pInvestorPosition->InstrumentID] = (pInvestorPosition->Position);
			//历史卖单
		}
		else
		{
			gTypeCheckState_S_Today[pInvestorPosition->InstrumentID] = true;
			gPosition_s_today[pInvestorPosition->InstrumentID] = (pInvestorPosition->Position);
			//今仓卖单 
		}
	}

	if (bIsLast)
	{
		for (int i = 0; i < TYPE_NUM; i++)
		{
			if(!gTypeCheckState_B_History[pInvestorPosition->InstrumentID])
				gPosition_b_history[pInvestorPosition->InstrumentID] = 0;
			if(!gTypeCheckState_B_Today[pInvestorPosition->InstrumentID])
				gPosition_b_today[pInvestorPosition->InstrumentID] = 0;
			if(!gTypeCheckState_S_History[pInvestorPosition->InstrumentID])
				gPosition_s_history[pInvestorPosition->InstrumentID] = 0;
			if(!gTypeCheckState_S_Today[pInvestorPosition->InstrumentID])
 				gPosition_s_today[pInvestorPosition->InstrumentID] = 0;
			//printf("品种：%s  BUY持仓(今仓)[%d] BUY持仓(历史仓)[%d] SELL持仓(今仓)[%d] SELL持仓(历史)[%d]\n", 
			//InstrumentID_n[i], Trade_dataA_Amount_B_Today[i], Trade_dataA_Amount_B_History[i], Trade_dataA_Amount_S_Today[i], Trade_dataA_Amount_S_History[i]);
		}
	}
	gPosition_s[pInvestorPosition->InstrumentID] = gPosition_s_today[pInvestorPosition->InstrumentID] + gPosition_s_history[pInvestorPosition->InstrumentID];
	gPosition_b[pInvestorPosition->InstrumentID] = gPosition_b_today[pInvestorPosition->InstrumentID] + gPosition_b_history[pInvestorPosition->InstrumentID];

	if (bIsLast)
	{
		//printf("%s ：总卖单[%d] 今卖单[%d] 非今日卖单[%d] 总买单[%d] 今买单[%d] 非今日买单[%d]\n", pInvestorPosition->InstrumentID,
	   // gPosition_s[pInvestorPosition->InstrumentID], gPosition_s_today[pInvestorPosition->InstrumentID], gPosition_s_history[pInvestorPosition->InstrumentID],
	   // gPosition_b[pInvestorPosition->InstrumentID], gPosition_b_today[pInvestorPosition->InstrumentID] , gPosition_b_history[pInvestorPosition->InstrumentID]);
		if (0)
		{
			map<string, int>::iterator iter_s;
			for (iter_s = gPosition_s.begin(); iter_s != gPosition_s.end(); iter_s++)
			{
				cout << iter_s->first << "->" << iter_s->second << endl;
			}

			map<string, int>::iterator iter_b;
			for (iter_b = gPosition_b.begin(); iter_b != gPosition_b.end(); iter_b++)
			{
				cout << iter_b->first << "->" << iter_b->second << endl;
			}

			map<string, int>::iterator iter_s_history;
			for (iter_s_history = gPosition_s_history.begin(); iter_s_history != gPosition_s_history.end(); iter_s_history++)
			{
				cout << iter_s_history->first << "->" << iter_s_history->second << endl;
			}

			map<string, int>::iterator iter_b_history;
			for (iter_b_history = gPosition_b_history.begin(); iter_b_history != gPosition_b_history.end(); iter_b_history++)
			{
				cout << iter_b_history->first << "->" << iter_b_history->second << endl;
			}

			map<string, int>::iterator iter_b_today;
			for (iter_b_today = gPosition_b_today.begin(); iter_b_today != gPosition_b_today.end(); iter_b_today++)
			{
				cout << iter_b_today->first << "->" << iter_b_today->second << endl;
			}

			map<string, int>::iterator iter_s_today;
			for (iter_s_today = gPosition_s_today.begin(); iter_s_today != gPosition_s_today.end(); iter_s_today++)
			{
				cout << iter_s_today->first << "->" << iter_s_today->second << endl;
			}
		}
	}

	//printf("CPP2 OnRspQryInvestorPosition %s %c %d   [%d]\n",pInvestorPosition->InstrumentID, pInvestorPosition->PosiDirection, 
	//	pInvestorPosition->Position, pInvestorPosition->SettlementID);
    PMsg(nThreadID_OnRspQryInvestorPosition, MY_OnRspQryInvestorPosition, pInvestorPosition, NULL, 0);
	EnterCriticalSection(&g_position);
	 char keystr[40] = { 0 };
	 _snprintf_s(keystr, sizeof(keystr), sizeof(keystr) - 1, "%s_%c", pInvestorPosition->InstrumentID, pInvestorPosition->PosiDirection);
	if (map_position.find(keystr) != map_position.end())
	{
		//找到
	}
	else
	{
		// 没找到该合约 
		if (pid < positionnum)
		{
			memcpy_s((PVOID)(&positionptr[0] + pid), sizeof(CThostFtdcInvestorPositionField), pInvestorPosition, sizeof(CThostFtdcInvestorPositionField));
			pid++;
		}
		pair<string, int>value(keystr, pid - 1);
		map_position.insert(value);
		//printf("c++a size: %d\n", (int)map_position.size());
	}
	LeaveCriticalSection(&g_position);
}

bool  IsErrorRspInfo2(CThostFtdcRspInfoField *pRspInfo)
{
	cerr << "--->>> " << "IsErrorRspInfo\n" << "0" << endl;	  //指针检查
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		cerr << "--->>> \nErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << endl;
		char errmsg[200] = { 0 };
		_snprintf_s(errmsg, sizeof(errmsg), sizeof(errmsg), "OnRspError ErrorID:%d ErrorMsg:%s", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
	} 
	return  ((pRspInfo) && (pRspInfo->ErrorID != 0));
}

double YestayAllAmount=0;
double TodayAllAmount=0;
double Available=0;

void CTDSpi::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pTradingAccount == NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		VNDEFTradeAcount tn;
		memset(&tn,0,sizeof(VNDEFTradeAcount));
		_snprintf_s(tn.BrokerID, sizeof(TThostFtdcBrokerIDType), sizeof(TThostFtdcBrokerIDType)-1,"%s", pTradingAccount->BrokerID);
		_snprintf_s(tn.InvestorID, sizeof(TThostFtdcAccountIDType), sizeof(TThostFtdcAccountIDType) - 1, "%s", pTradingAccount->AccountID);
		_snprintf_s(tn.TradingDay, sizeof(TThostFtdcDateType), sizeof(TThostFtdcDateType) - 1, "%s", pTradingAccount->TradingDay);
		//可取资金:pTradingAccount->WithdrawQuota
		//静态权益=上日结算-出金金额+入金金额
		tn.prebalance = pTradingAccount->PreBalance - pTradingAccount->Withdraw + pTradingAccount->Deposit;
		//cerr << "--->>> 静态权益: " << preBalance  << endl;
		//动态权益=静态权益+ 平仓盈亏+ 持仓盈亏- 手续费
		tn.current = tn.prebalance + pTradingAccount->CloseProfit + pTradingAccount->PositionProfit - pTradingAccount->Commission;
		tn.rate =( (int)(10000* (tn.current - tn.prebalance) / tn.prebalance))/100;
		tn.available = pTradingAccount->Available; 
		tn.positionrate = 100*(tn.current - tn.available )/ tn.current ;
		tn.WithdrawQuota = pTradingAccount->WithdrawQuota;
		tn.Commission = pTradingAccount->Commission;
		YestayAllAmount = tn.prebalance;  
		TodayAllAmount = tn.current;  
		Available = tn.available;
		PMsg(nThreadID_OnRspQryTradingAccount, MY_OnRspQryTradingAccount, &tn, pRspInfo , nRequestID);		
	}
}

///请求查询合约响应
void CTDSpi::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{	
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pInstrument == NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
	//	printf("乘数 InstrumentID[%s] UnderlyingMultiple[%d]\n\n", pInstrument->InstrumentID, pInstrument->UnderlyingMultiple  );
       gUnderlyingMultiple[pInstrument->InstrumentID] =  pInstrument->UnderlyingMultiple;
	}
}

void CTDSpi::OnRspQryInvestorProductGroupMargin(CThostFtdcInvestorProductGroupMarginField *pInvestorProductGroupMargin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pInvestorProductGroupMargin == NULL)
	{
		return;
	}
	printf("\nBrokerID[%s]  UseMargin[%0.04f] LongUseMargin[%0.04f]  ShortUseMargin[%0.04f] ExchMargin[%0.08f] LongExchMargin[%0.08f] ShortExchMargin[%0.08f] Commission[%0.08f]\n",
		pInvestorProductGroupMargin->BrokerID,
		pInvestorProductGroupMargin->UseMargin,
		pInvestorProductGroupMargin->LongUseMargin,
		pInvestorProductGroupMargin->ShortUseMargin,
		pInvestorProductGroupMargin->ExchMargin,
		pInvestorProductGroupMargin->LongExchMargin,
		pInvestorProductGroupMargin->ShortExchMargin,
		pInvestorProductGroupMargin->Commission);
}

void CTDSpi::OnRspQryInstrumentMarginRate(CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;

	if (pInstrumentMarginRate == NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		 gMarginRate_long[pInstrumentMarginRate->InstrumentID] = pInstrumentMarginRate->LongMarginRatioByMoney;// LongMarginRatioByMoney;// gPosition_s_today[pInstrumentMarginRate->InstrumentID] + gPosition_s_history[pInstrumentMarginRate->InstrumentID];
		 gMarginRate_short[pInstrumentMarginRate->InstrumentID] = pInstrumentMarginRate->ShortMarginRatioByMoney;// gPosition_s_today[pInstrumentMarginRate->InstrumentID] + gPosition_s_history[pInstrumentMarginRate->InstrumentID];
	}
}

void CTDSpi::OnRspQryInstrumentCommissionRate(CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;

	if (pInstrumentCommissionRate == NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		pInstrumentCommissionRate->BrokerID;
		pInstrumentCommissionRate->CloseRatioByMoney;
		pInstrumentCommissionRate->CloseRatioByVolume;
		pInstrumentCommissionRate->CloseTodayRatioByMoney;
		pInstrumentCommissionRate->CloseTodayRatioByVolume;
		pInstrumentCommissionRate->InstrumentID;
		pInstrumentCommissionRate->InvestorID;
		pInstrumentCommissionRate->InvestorRange;
		pInstrumentCommissionRate->OpenRatioByMoney;
	}
}

///查询最大报单数量响应
void CTDSpi::OnRspQueryMaxOrderVolume(CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;

	if (pQueryMaxOrderVolume == NULL)
	{
		return;
	}
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{

	}
}

void CTDSpi::OnRspQryAccountregister(CThostFtdcAccountregisterField *pAccountregister, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pAccountregister == NULL)
	{
		return;
	}
	printf("交易日期：%s\n", pAccountregister->TradeDay);
	printf("银行编码：%s\n", pAccountregister->BankID);
	printf("银行分支机构编码：%s\n", pAccountregister->BankBranchID);
	printf("银行帐号：%s\n", pAccountregister->BankAccount);
	printf("期货公司编码：%s\n", pAccountregister->BrokerID);
	printf("期货公司分支机构编码：%s\n", pAccountregister->BrokerBranchID);
	printf("投资者帐号：%s\n", pAccountregister->AccountID);
	printf("证件类型：%c\n", pAccountregister->IdCardType);
	printf("证件号码：%s\n", pAccountregister->IdentifiedCardNo);

	printf("客户姓名：%s\n", pAccountregister->CustomerName);
	printf("币种代码：%s\n", pAccountregister->CurrencyID);
	printf("开销户类别：%c\n", pAccountregister->OpenOrDestroy);
}

///期货发起银行资金转期货通知
void CTDSpi::OnRtnFromBankToFutureByFuture(CThostFtdcRspTransferField *pRspTransfer)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pRspTransfer == NULL)
	{
		return;
	}
}


///期货发起期货资金转银行通知
void CTDSpi::OnRtnFromFutureToBankByFuture(CThostFtdcRspTransferField *pRspTransfer)
{
	if (showlog)
		cerr << __FUNCTION__ << std::endl;
	if (pRspTransfer == NULL)
	{
		return;
	}
}

