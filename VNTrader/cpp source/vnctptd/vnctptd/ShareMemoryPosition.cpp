#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "CTP/ThostFtdcUserApiStruct.h"
#include "ShareMemoryPosition.h"
#include <string>
using namespace std;

int pid = 1;
HANDLE hMapFile = NULL;
LPCTSTR pBuf = NULL;

double CShareMemoryPosition::InitPositionData(char *InstrumentID)
{
	char sharename[60] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTraderPosition%d", pid);
	hMapFile = CreateFileMappingA(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD)
		sizeof(PositionDataType)*MAX_INSUTRUMENT,                // maximum object size (low-order DWORD)
		sharename);                 // name of mapping object

	if (hMapFile == NULL)
	{
		printf(TEXT("Could not create file mapping object (%d).\n"),
			GetLastError());
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile,   // handle to map object
			FILE_MAP_ALL_ACCESS, // read/write permission
			0,
			0,
			sizeof(PositionDataType)*MAX_INSUTRUMENT);
	}
	if (pBuf == NULL)
	{
		printf(TEXT("Could not map view of file (%d).\n"),
			GetLastError());
		CloseHandle(hMapFile);
		return 0;
	}

	memset((PVOID)pBuf, 0, sizeof(PositionDataType)*MAX_INSUTRUMENT);

	return 1;
}


void * CShareMemoryPosition::GetPosition(int id)
{
	char sharename[100] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTraderPosition%d", pid);
	if (hMapFile == NULL)
	{
		hMapFile = OpenFileMappingA(
			FILE_MAP_ALL_ACCESS,   // read/write access
			FALSE,                 // do not inherit the name
			sharename);               // name of mapping object
	}
	if (hMapFile == NULL)
	{
		//_tprintf(TEXT("Could not open file mapping object (%d).\n"),
		//	GetLastError());
		//ReleaseMutex(hMutex2);
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
			FILE_MAP_ALL_ACCESS,  // read/write permission
			0,
			0,
			sizeof(PositionDataType)*MAX_INSUTRUMENT);
	}
	if (pBuf == NULL)
	{
		//_tprintf(TEXT("Could not map view of file (%d).\n"),
		//	GetLastError());
		CloseHandle(hMapFile);
		//ReleaseMutex(hMutex2);
		return 0;
	}
	//	PositionDataType(*tn)[MAX_INSUTRUMENT] = (PositionDataType(*)[MAX_INSUTRUMENT])pBuf;

	PositionDataType(*tn)  = (PositionDataType(*) )pBuf;
	/*
	for (int i = 0; i < 10; i++)
	{
	printf("[%d] %s , %f , %f\n", i, tn[0][i].InstrumentID, tn[0][i].KlineTime, tn[0][i].HighestPrice);
	}*/

	//int id = gMarket[a->InstrumentID];
	return  &tn[id];
}

int CShareMemoryPosition::GetPositionNum()
{
 	return  (int)map_position.size();
}



double  CShareMemoryPosition::UpdatePosition(string instrumentKey, int i, int id)
{
	char sharename[100] = { 0 };
	_snprintf_s(sharename, sizeof(sharename), sizeof(sharename) - 1, "Local\\VNTraderPosition%d", pid);
	if (hMapFile == NULL)
	{
		hMapFile = OpenFileMappingA(
			FILE_MAP_ALL_ACCESS,
			FALSE,
			sharename);
	}
	if (hMapFile == NULL)
	{
		//_tprintf(TEXT("Could not open file mapping object (%d).\n"),
		//	GetLastError());
		//ReleaseMutex(hMutex2);
		return 0;
	}
	if (pBuf == NULL)
	{
		pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
			FILE_MAP_ALL_ACCESS,  // read/write permission
			0,
			0,
			sizeof(PositionDataType)*MAX_INSUTRUMENT);
	}
	if (pBuf == NULL)
	{
		//_tprintf(TEXT("Could not map view of file (%d).\n"),
		//	GetLastError());
		CloseHandle(hMapFile);
		//ReleaseMutex(hMutex2);
		return 0;
	}
	PositionDataType(*tn)[MAX_INSUTRUMENT] = (PositionDataType(*)[MAX_INSUTRUMENT])pBuf;
	/*
	PositionDataType tt;
	memset(&tt, 0, sizeof(tt));
	tt.KlineTime = 0.1500;
	_snprintf_s(tt.InstrumentID, sizeof(tt.InstrumentID), sizeof(tt.InstrumentID) - 1, "%s", instrumentKey.c_str());
	memcpy_s((PVOID)(tn[0] + id), sizeof(PositionDataType), &tt, sizeof(PositionDataType));
	*/
	memcpy_s((PVOID)(tn[0] + id), sizeof(PositionDataType), (const void *const)&(positiondata[id]), sizeof(PositionDataType));
	//ReleaseMutex(hMutex2);
	//UnmapViewOfFile(pBuf);
	//CloseHandle(hMapFile);
	return  1;
}


 