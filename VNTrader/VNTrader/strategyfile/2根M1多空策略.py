from CTPTraderType import *
import globalvar


def checkinstrumentID(marketdata, strategyname):
    if  strategyname in globalvar.dict_strategyinstrument:
        if  str(marketdata.contents.InstrumentID, encoding="utf-8") in globalvar.dict_strategyinstrument[strategyname]:
            return 0
        else:
            return 1
    else:
        return 1
#通过Python调用C++方法获取K线M1数据范例，Python可根据M1周期生成其他周期数据
def OnTick(marketdata,  strategyname):
    print('OnTick 2bar:'+str(marketdata.contents.InstrumentID, encoding="utf-8"))

    print(__name__)
    global dict_strategyinstrument, dick_tick
    # 检查是否是本策略选中需要交易的合约编码，如果不在则返回，配置在策略文件名称一致的csv文件中，本策略是strategy1.csv，
    if checkinstrumentID(marketdata, strategyname):
        return

    if globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Close>globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Open and globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Close > globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Open:
        print("买入" + str(marketdata.contents.InstrumentID, encoding="utf-8"))
        # 策略池功能完善中
        # 用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
        exchangeid = globalvar.dict_exchange[str(marketdata.contents.InstrumentID, encoding="utf-8")].split(',')[0]
        result = globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid, '0', '1',
                                          VN_OPT_LimitPrice, marketdata.contents.LastPrice + 10, 1)
        result = result + globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid,
                                                   '0', '0', VN_OPT_LimitPrice, marketdata.contents.LastPrice + 10, 1)
        if result == 0:
            print('报单成功')
        else:
            print('报单失败')
    elif globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Close>globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Open and globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Close > globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Open:
        print("卖出" + str(marketdata.contents.InstrumentID, encoding="utf-8"))
        # 策略池功能完善中
        # 用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
        exchangeid = globalvar.dict_exchange[str(marketdata.contents.InstrumentID, encoding="utf-8")].split(',')[0]
        result = globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid, '1', '1',
                                          VN_OPT_LimitPrice, marketdata.contents.LastPrice - 10, 1)
        result = result + globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid,
                                                   '1', '0', VN_OPT_LimitPrice, marketdata.contents.LastPrice - 10, 1)
        if result == 0:
            print('报单成功')
        else:
            print('报单失败')

    '''
    print("M1 K线合约名称 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.KlineTime))
    print("M1 K线 最高价 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.High))
    print("M1 K线 最低价 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.Low))
    print("M1 K线 收盘价 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.Close))
    print("M1 K线 成交量 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.Volume))
    print("M1 K线 交易日 ref(0): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 0).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.KlineTime))
    print("M1 K线 最高价 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.High))
    print("M1 K线 最低价 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Low))
    print("M1 K线 收盘价 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Close))
    print("M1 K线 成交量 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.Volume))
    print("M1 K线 交易日 ref(1): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 1).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.KlineTime))
    print("M1 K线 最高价 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.High))
    print("M1 K线 最低价 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Low))
    print("M1 K线 收盘价 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Close))
    print("M1 K线 成交量 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.Volume))
    print("M1 K线 交易日 ref(2): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 2).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.KlineTime))
    print("M1 K线 最高价 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.High))
    print("M1 K线 最低价 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.Low))
    print("M1 K线 收盘价 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.Close))
    print("M1 K线 成交量 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.Volume))
    print("M1 K线 交易日 ref(3): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 3).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.KlineTime))
    print("M1 K线 最高价 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.High))
    print("M1 K线 最低价 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.Low))
    print("M1 K线 收盘价 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.Close))
    print("M1 K线 成交量 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.Volume))
    print("M1 K线 交易日 ref(4): " + str(globalvar.md.GetKline(marketdata.contents.InstrumentID, 4).contents.TradingDay, encoding="utf-8"))
    '''