#策略： 连续5比Tick上涨买入，连续5比Tick下跌卖出
from CTPTraderType import *
import globalvar
global dict_strategyinstrument
global dick_tick
dick_tick = {}

def checkinstrumentID(marketdata, strategyname):
    if  strategyname in globalvar.dict_strategyinstrument:
        if  str(marketdata.contents.InstrumentID, encoding="utf-8") in globalvar.dict_strategyinstrument[strategyname]:
            return 0
        else:
            return 1
    else:
        return 1

def OnTick(marketdata, strategyname):
    print(__name__)
    global dict_strategyinstrument, dick_tick
    #检查是否是本策略选中需要交易的合约编码，如果不在则返回，配置在策略文件名称一致的csv文件中，本策略是strategy1.csv，
    if checkinstrumentID(marketdata, strategyname):
        return

    print('合约' + str(marketdata.contents.InstrumentID, encoding="utf-8")+'在策略'+strategyname+'生效')
    if marketdata.contents.InstrumentID in dick_tick:
        pass
    else:
        list_tick = []
        dick_tick[marketdata.contents.InstrumentID] = list_tick
        print(str(marketdata.contents.InstrumentID, encoding="utf-8"))

    dick_tick[marketdata.contents.InstrumentID].insert(0, marketdata.contents.LastPrice)
    if len(dick_tick[marketdata.contents.InstrumentID])>5:
        print('运行策略('+strategyname+'.py)： '+str(marketdata.contents.InstrumentID, encoding="utf-8")+ ',' +str(marketdata.contents.LastPrice))
        print("比较" + str(marketdata.contents.InstrumentID, encoding="utf-8") + ',' +str(dick_tick[marketdata.contents.InstrumentID][0]) + ','+
        str(dick_tick[marketdata.contents.InstrumentID][1]) + ','+ str(dick_tick[marketdata.contents.InstrumentID][2]))
        if dick_tick[marketdata.contents.InstrumentID][4]>1e-7 and dick_tick[marketdata.contents.InstrumentID][0] > dick_tick[marketdata.contents.InstrumentID][1] and  dick_tick[marketdata.contents.InstrumentID][1] > dick_tick[marketdata.contents.InstrumentID][2] and dick_tick[marketdata.contents.InstrumentID][2] > dick_tick[marketdata.contents.InstrumentID][3] and dick_tick[marketdata.contents.InstrumentID][3] > dick_tick[marketdata.contents.InstrumentID][4]:
            print("买入"+str(marketdata.contents.InstrumentID, encoding="utf-8"))
            #策略池功能完善中
            #用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
            exchangeid = globalvar.dict_exchange[str(marketdata.contents.InstrumentID, encoding="utf-8")].split(',')[0]

            result = globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid, '0', '1',VN_OPT_LimitPrice , marketdata.contents.LastPrice+10, 1)
            result = result + globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid, '0', '0',VN_OPT_LimitPrice , marketdata.contents.LastPrice+10, 1)
            if result ==0:
                print('报单成功')
            else:
                print('报单失败')

        elif dick_tick[marketdata.contents.InstrumentID][4]>1e-7 and dick_tick[marketdata.contents.InstrumentID][0] < dick_tick[marketdata.contents.InstrumentID][1] and dick_tick[marketdata.contents.InstrumentID][1] < dick_tick[marketdata.contents.InstrumentID][2] and dick_tick[marketdata.contents.InstrumentID][2] < dick_tick[marketdata.contents.InstrumentID][3] and dick_tick[marketdata.contents.InstrumentID][3] < dick_tick[marketdata.contents.InstrumentID][4]:
            print("卖出"+str(marketdata.contents.InstrumentID, encoding="utf-8"))
            # 策略池功能完善中
            #用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
            exchangeid = globalvar.dict_exchange[str(marketdata.contents.InstrumentID, encoding="utf-8")].split(',')[0]
            result = globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid, '1', '1',VN_OPT_LimitPrice , marketdata.contents.LastPrice-10, 1)
            result = result + globalvar.td.InsertOrder(str(marketdata.contents.InstrumentID, encoding="utf-8"), exchangeid,'1', '0',VN_OPT_LimitPrice , marketdata.contents.LastPrice-10, 1)
            if result ==0:
                print('报单成功')
            else:
                print('报单失败')

    #print( globalvar.get_value('key1') + globalvar.get_value('key2'))
