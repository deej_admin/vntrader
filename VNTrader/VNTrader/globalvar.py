# -*- coding: utf-8 -*-
#本页面用于多个py文件之间共享全局变量
#用于保存选择框变量
global list_INE, list_CFFEX, list_SHFE, list_DCE, list_CZCE
global dict_exchange, dict_instrument
global tradestate
#保存策略执行的合约编码
global dict_strategyinstrument
#保存当前pygraph K线图、闪电图选中选中合约对应的交易所，合约编码，周期
global selectperiod, selectexchange, selectinstrumenid
#实例化交易库和行情库作为全局遍历保存
global td, md
td = 0
md = 0
selectinstrumenid = '1'
selectperiod=1
def _init():  # 初始化
    global tradestate
    global td, md
    global list_INE, list_CFFEX, list_SHFE, list_DCE, list_CZCE, selectperiod, selectexchange, selectinstrumenid
    global dict_strategyinstrument
    tradestate = 0
    list_INE = []
    list_CFFEX = []
    list_SHFE = []
    list_DCE = []
    list_CZCE = []
    global dict_exchange,dict_instrument
    dict_exchange= {}
    dict_instrument= {}
    dict_strategyinstrument = {}
    global _global_dict
    _global_dict = {}

def getklineperiod():
    global selectperiod
    return selectperiod

def caompareinstrumwent(InstrumentID):
    global selectinstrumenid
    #print(selectinstrumenid+', '+InstrumentID)
    if  selectinstrumenid == InstrumentID:
        return True
    else:
        return False

def set_value(key, value):
    """ 定义一个全局变量 """
    _global_dict[key] = value


def get_value(key, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return _global_dict[key]
    except:
        pass
        #KeyError

def set_list_INE(value):
    """ 定义一个全局变量 """
    #list_INE[id] = value
    list_INE.append(value)
def get_list_INE(id, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return list_INE[id]
    except:
        pass
        #KeyError

def getlen_list_INE():
    return len(list_INE)

def set_list_CFFEX(value):
    """ 定义一个全局变量 """
    list_CFFEX.append(value)

def get_list_CFFEX(id, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return list_CFFEX[id]
    except:
        pass
        #KeyError

def getlen_list_CFFEX():
    return len(list_CFFEX)

def set_list_SHFE(value):
    """ 定义一个全局变量 """
    list_SHFE.append(value)

def get_list_SHFE(id, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return list_SHFE[id]
    except:
        pass
        #KeyError

def getlen_list_SHFE():
    return len(list_SHFE)

def set_list_DCE(value):
    """ 定义一个全局变量 """
    list_DCE.append(value)


def get_list_DCE(id, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return list_DCE[id]
    except:
        pass
        #KeyError

def getlen_list_DCE():
    return len(list_DCE)

def set_list_CZCE(value):
    """ 定义一个全局变量 """
    list_CZCE.append(value)

def get_list_CZCE(id, defValue=None):
    """ 获得一个全局变量,不存在则返回默认值 """
    try:
        return list_CZCE[id]
    except:
        pass
        #KeyError

def getlen_list_CZCE():
    return len(list_CZCE)
