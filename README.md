![Image text](https://images.gitee.com/uploads/images/2021/0624/110700_4275874f_1204097.png)

# VNPY官方 VNTrader 
（基于期货CTP接口专用量化交易开源软件）



#### 介绍
VNTrader是VNPY官方 http://www.vnpy.cn 推出的一款国内期货量化交易开源软件，

VNTrader官网 http://www.vntrader.cn

《VNPY官方发布VNTrader期货CTP框架开发环境配置快速入门教程》 https://zhuanlan.zhihu.com/p/388316382

《VNTrader开源项目采用Github Desktop差异更新代码步骤》https://zhuanlan.zhihu.com/p/386181364

《VNPY官方VNTrader 开源，采用Python + PyQT + ctypes + PyQTGraph，是CTP优秀Python框架》https://zhuanlan.zhihu.com/p/382754307

《VNPY官方新架构VNTrader期货CTP接口Python开源框架共同开发者邀请》 https://zhuanlan.zhihu.com/p/390476717

《VNPY官方发布VNTrader的EMA策略实现》https://zhuanlan.zhihu.com/p/392796443


基于GPLV3开源协议，任何机构和个人可以免费下载和使用，无需付费。

注意，需要在期货开盘时间前后20分钟，放开登录CTP接口服务器
期货开盘时间  9:00-11:30   ,1:30 - 15:00   ,  21:00-2:30

仿真账户支持 (支持股指期货、股指期权、商品期货、商品期权仿真交易)
 (只能工作日白天访问网址，其他时间网站关闭)
http://www.simnow.com.cn

开立实盘账户(A级期货公司，优惠一步到位)
 http://www.kaihucn.cn 

基于CTP接口的开源性，打破收费软件垄断，采用VNTrader开源项目也可解决自己造轮子导致周期长门槛高的问题。
VNTrader是专门针对商品期货CTP接口的GUI窗口程序，支持多个Python策略组成策略池，支持回测，支持多周期量化交易。

VNTrader客户端开源代码 VNTrader是VNPY官方提供的CTP开源项目客户端源代码，
支持国内149家期货公司的CTP接入，
支持股指期货，股指期权、商品期货、商品期权的程序化交易和量化交易的仿真回测。

全新架构，性能再次升级，python的便捷,C++性能加持，比老版本更好用，性能提升300%以上，全新系统命名未VNTrader，属于VNPY官方发布的重点全新架构的产品。


VNTrader的Python和底层C++代码全部开源， 这个是一个有具大性能提升大版本


VNPY官方网站 http://www.vnpy.cn 


官方QQ群： 256163463




![VNPY官方发布全新一代期货CTP框架，Python框架VNTrader](https://images.gitee.com/uploads/images/2021/0624/111454_46c70c7a_1204097.png "VNPY.png")


![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/111503_6980ce37_1204097.jpeg "bird.jpg")


![CTP接口支持交易和期货公司](https://images.gitee.com/uploads/images/2021/0624/112928_eea13eb4_1204097.png "s1.png")

![VNTrader CTP接口Python开源框架架构图](https://images.gitee.com/uploads/images/2021/0624/112936_c222d986_1204097.png "S2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/002542_d3196b86_1204097.png "架构图.png")





![输入图片说明](https://images.gitee.com/uploads/images/2021/0709/102816_afa6da37_1204097.png "window.png")




![VNTrader委托记录，成交记录，持仓记录](https://images.gitee.com/uploads/images/2021/0624/143814_4ecd69e6_1204097.png "S4.png")

![VNTrader期货账户详情](https://images.gitee.com/uploads/images/2021/0624/143826_1afee0ca_1204097.png "S5.png")

![VNTrader添加期货账户](https://images.gitee.com/uploads/images/2021/0624/143835_563f3c7c_1204097.png "S6.png")

![VNTrader参数优化图](https://images.gitee.com/uploads/images/2021/0624/143847_fa6b1eb8_1204097.png "S7.png")

![VNTrader资金曲线记录](https://images.gitee.com/uploads/images/2021/0624/143905_68094eda_1204097.png "S8.png")
![VNTrader资金曲线记录](https://images.gitee.com/uploads/images/2021/0629/121350_8a0920d4_1204097.png "VNPY")
![VNTrader资金曲线记录](https://images.gitee.com/uploads/images/2021/0629/121446_2e3b0694_1204097.png "VNTrader")


重点：
在未来 VNTrader 将继承http://www.virtualapi.cn 的强大功能，具体可以见 http://www.gucps.cn
完全不同于历史老版本，这个版本不仅性能优异，开源，而且结合C++的特点，结合底层仿真（获得国家发明专利）成为程序化交易最佳利器。

抛弃历史曾出现的大杂烩版本，专门面向国内商品期货、股指期货实现程序化交易CTP接口的专属版本，符合“精简、高性能、精细化回测、功能强大、入门更容易”等特点。


![VNTrader](https://images.gitee.com/uploads/images/2021/0630/031421_78bc8a86_1204097.png "vnpy.png")




安装Anaconda 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/000502_c49a5e80_1204097.jpeg "p0.jpg")


我们还需在Anaconda 内部安装相应插件

VNTrader需要的插件有：

PyQT5 , pyqtgraph , numpy , pandas,

选择红圈内的VNTrader标签，在红圈搜索pyqt，把这些插件都安装上

![安装Python插件](https://images.gitee.com/uploads/images/2021/0726/000259_8a9844fc_1204097.png "安装Python插件")

安装Pandas库

![安装Pandas库](https://images.gitee.com/uploads/images/2021/0726/000324_5183989e_1204097.png "安装Pandas库")



#### 软件架构
软件架构说明

需要安装的模块
Python3.0 + PyQT5 +pyqtgraph + numpy+ pandas + qdarkstyle


python下载

https://www.python.org/

Pycharm下载

https://www.jetbrains.com/pycharm/

除了通过Pycharm安装模块外（有时，点击 “插件”->" +" 不能正常显示可安装的模块），也可以通过anacoda安装模块
https://www.anaconda.com/

默认英文版，可安装中文版本插件

注意：Python、IDE、VNTrader DLL模块必须一致，必须同时是32位或同时是64位。

支持Windows平台

#### 安装教程

1.  安装Python3.0
2.  安装Pycharm
3.  在Pycharm安装PyQT插件
4.  在Pycharm 中菜单 “运行”-> "运行"

#### 使用说明

VNTrader是VNPY官方 http://www.vnpy.cn 推出的一款国内期货量化交易开源软件，
主要支持CTP接口，支持国内149家期货公司程序化交易，实现程序化交易是免费的。
支持股指期货、商品期货、股指期权、商品期权，
支持中国8大合规交易所中的5所，包括上海期货交易所，大连期货交易所、
郑州期货交易所、中金所、能源所。

#### 目录说明：

strategy  策略存放目录

temp CTP接口产生的临时流文件存放目录

setting.ini 账户和服务器配置文件

thostmduserapi_se.dll  CTP接口原生行情接口；

thosttraderapi_se.dll    CTP接口原生交易接口；

vnctpmd.dll  CTP接口原生交易接口的代理库，用于和ctypes方式封装的CTPMarket.py 引用；

vnctptd.dll    CTP接口原生交易接口的代理库，用于和ctypes方式封装的CTPTrader 引用；

CTPMarket.py           Python ctypes 方式封装；

CTPTrader.py            Python ctypes 方式封装；

CTPMarketType.py    Python类型定义；

CTPTraderType.py     Python类型定义；

VNTrader.py  基于PyQT5的GUI程序；

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


